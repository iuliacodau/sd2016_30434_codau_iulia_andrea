package studpack;

public interface CursDAO {

	public void insertCurs(Curs c);
	public Curs selectCurs(int id);
	public void updateCurs(int id, String nume);
	public void deleteCurs(int id);
}
