package studpack;

public class Curs {

	private int c_id;
	private String nume;
	private int credite;
	
	Curs(){
		
	}
	
	Curs(int cid, String nume, int c){
		this.c_id=cid;
		this.nume=nume;
		this.credite=c;
	}
	
	public int getId(){
		return c_id;
	}
	
	public void setId(int id){
		this.c_id=id;
	}
	
	public String getName(){
		return nume;
	}
	
	public void setNume(String nume){
		this.nume=nume;
	}
	
	public int getCredite(){
		return credite;
	}
	
	public void setCredite(int c){
		this.credite=c;
	}
}
