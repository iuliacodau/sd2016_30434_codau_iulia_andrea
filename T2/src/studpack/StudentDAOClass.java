package studpack;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StudentDAOClass implements StudentDAO{
	
	Connection connection=null;
	Statement st=null;
	ResultSet rs=null;

	public StudentDAOClass(){
		
	}
	
	private Connection getConnection() throws SQLException{
		Connection conn;
		conn=ConnectionFactory.getInstance().getConnection();
		return conn;
	}

	@Override
	public void insertStudent(Student s) {
		try{
			String qs="INSERT INTO student VALUES ('"+s.getNume()+"','"+s.getPrenume()+"','"+s.getId()+"', '"+s.getDataNasterii()+"' , '"+s.getCid()+"')";
			connection=getConnection();
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data inserted: " + s.getNume()+" "+s.getPrenume()+" "+s.getId()+" "+s.getDataNasterii()+" "+s.getCid() );
		}catch (SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public Student selectStudent(int id) {
		Student stud=new Student();
		try{
			String qs="SELECT * FROM student WHERE S_ID='"+id+"'";
			connection=getConnection();
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				stud.setNume(rs.getString("Nume"));
				stud.setPrenume(rs.getString("Prenume"));
				stud.setDataNasterii(rs.getString("Data_Nasterii"));
				stud.setId(rs.getInt("S_ID"));
				stud.setCid(rs.getInt("C_ID"));
			}
			System.out.println("Data selected " + stud.getNume() +" "+ stud.getPrenume()+ " "+stud.getId()+" "+ stud.getDataNasterii()+" "+stud.getCid());
		}catch (SQLException e){
			e.printStackTrace();
		}
		return stud;
	}

	@Override
	public void updateStudent(int id, String prenume) {
		try{
			String qs="UPDATE student SET Prenume='"+prenume+"' WHERE S_ID='"+id+"'";
			connection=getConnection();
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + prenume);
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteStudent(int id) {
		try{
			String qs="DELETE FROM student WHERE S_ID='"+id+"'";
			connection=getConnection();
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data deleted: "+id);
		}catch (SQLException e){
			e.printStackTrace();
		}	
	}
}
