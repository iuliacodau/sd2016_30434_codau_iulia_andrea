package studpack;

public interface StudentDAO {

	public void insertStudent(Student s);
	public Student selectStudent(int id);
	public void updateStudent(int id, String pn);
	public void deleteStudent(int id);
}
