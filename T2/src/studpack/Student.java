package studpack;

public class Student {
	
	private String nume;
	private String prenume;
	private int s_id;
	private String data_nasterii;
	private int c_id;
	
	Student(){
		
	}

	public Student(String nume,String prenume, int id, String dn, int cid){
		this.nume=nume;
		this.prenume=prenume;
		this.s_id=id;
		this.data_nasterii=dn;
		this.c_id=cid;
	}
	
	public String getNume(){
		return nume;
	}
	
	public void setNume(String nume){
		this.nume=nume;
	}
	
	public String getPrenume(){
		return prenume;
	}
	
	public void setPrenume(String prenume){
		this.prenume=prenume;
	}
	
	public String getDataNasterii(){
		return data_nasterii;
	}
	
	public void setDataNasterii(String dn){
		this.data_nasterii=dn;
	}
	
	public int getId(){
		return s_id;
	}
	
	public void setId(int sid){
		this.s_id=sid;
	}
	
	public void setCid(int cid){
		this.c_id=cid;
	}
	
	public int getCid(){
		return c_id;
	}
}
