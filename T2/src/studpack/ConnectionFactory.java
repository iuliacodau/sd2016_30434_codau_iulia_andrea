package studpack;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class ConnectionFactory {

	private static ConnectionFactory connectionFactory=null;

	private ConnectionFactory(){
	try{
		Class.forName("com.mysql.jdbc.Driver");
	}catch (ClassNotFoundException e){
		e.printStackTrace();
	}
	}
	
	public Connection getConnection() throws SQLException{
		Connection conn=null;
		conn=(Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/facultate?autoReconnect=true&useSSL=false", "root", "root");
		
		if (conn!=null){
			System.out.println("Connected to database");
		}
		return conn;	
	}
	
	public static ConnectionFactory getInstance(){
		if (connectionFactory==null){
			connectionFactory=new ConnectionFactory();
		}
		return connectionFactory;
	}
}

