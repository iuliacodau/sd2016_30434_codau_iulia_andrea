package studpack;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CursDAOClass implements CursDAO{

	Connection connection=null;
	Statement st=null;
	ResultSet rs=null;

	public CursDAOClass(){
		
	}
	
	private Connection getConnection() throws SQLException{
		Connection conn;
		conn=ConnectionFactory.getInstance().getConnection();
		return conn;
	}
	
	@Override
	public void insertCurs(Curs c) {
		try{
			String qs="INSERT INTO curs VALUES ('"+c.getId()+"','"+c.getName()+"','"+c.getCredite()+"')";
			connection=getConnection();
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data inserted: " + c.getId()+ c.getName()+c.getCredite());
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public Curs selectCurs(int id) {
		Curs curs=new Curs();
		try{
			String qs="SELECT * FROM curs WHERE C_ID='"+id+"'";
			connection=getConnection();
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				curs.setId(rs.getInt("C_ID"));
				curs.setNume(rs.getString("Nume"));
				curs.setCredite(rs.getInt("Credite"));
			}
			System.out.println("Data selected" + curs.getId() + curs.getName() + curs.getCredite());
		}catch (SQLException e){
			e.printStackTrace();
		}
		return curs;
	}

	@Override
	public void updateCurs(int id, String nume) {
		try{
			String qs="UPDATE curs SET Nume='"+nume+"' WHERE C_ID='"+id+"'";
			connection=getConnection();
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + nume);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void deleteCurs(int id) {
		try{
			String qs="DELETE FROM curs WHERE C_ID='"+id+"'";
			connection=getConnection();
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data deleted: "+id);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}

}
