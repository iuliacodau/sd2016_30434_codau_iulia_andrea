package studpack;

import static org.junit.Assert.*;


import org.junit.Test;

public class studtest {
	
	/*
	@Test
	public void testStud(){
		StudentDAOClass st=new StudentDAOClass();
		st.insertStudent(new Student("Galceava", "Ana", 4, "13-02-1995", 1));
		Student s=new Student();
		s=st.selectStudent(4);
		assertEquals("Galceava", s.getNume());
		assertEquals("Ana", s.getPrenume());
		assertEquals(4, s.getId());
		assertEquals("13-02-1995", s.getDataNasterii());
		assertEquals(1, s.getCid());
		st.updateStudent(4, "Maria");
		s=st.selectStudent(4);
		assertEquals("Maria", s.getPrenume());
		st.deleteStudent(16);
		s=st.selectStudent(16);
		assertEquals(null, s.getPrenume());
	}
	/
	@Test
	public void deleteStudent(){
	StudentDAOClass st=new StudentDAOClass();
	Student s=new Student();
	st.deleteStudent(16);
	s=st.selectStudent(16);
	assertEquals(null, s.getPrenume());
}*/
	
	@Test
	public void testCurs(){
		CursDAOClass cs=new CursDAOClass();
		cs.insertCurs(new Curs(5, "Proiectare",5));
		Curs c=new Curs();
		c=cs.selectCurs(2);
		assertEquals("Fizica", c.getName());
		assertEquals(2, c.getId());
		assertEquals(5, c.getCredite());
		cs.updateCurs(2, "Fizica cuantica");
		c=cs.selectCurs(2);
		assertEquals("Fizica cuantica", c.getName());
		//cs.deleteCurs(4);
		//c=cs.selectCurs(4);
		//assertEquals(null, s.getName());
	}
	
	//@Test
	/*public void testInsert() {
		StudentDAOClass st=new StudentDAOClass();
		Student s=new Student("Marius", "Popescu", 21, "22-09-1994");
		st.insertStudent(s);
		assertEquals(22, s.getId());
	}*/

	/*public void testUpdateStud() {
		
		StudentDAOClass st=new StudentDAOClass();
		st.updateStudent(22, "Cristi");
	}
	public void testSelectStud() {
		StudentDAOClass st=new StudentDAOClass();
		Student s=new Student();
		s=st.selectStudent(22);
		System.out.println("Prenule "+s.getPrenume());
		System.out.println("Nume "+s.getNume());
		System.out.println("Prenule "+s.getDataNasterii());
		System.out.println("Prenule "+s.getId());
		Student s2=new Student("Codau", "Iulia", 23, null);
		//Date d1=s.getDataNasterii();
		//System.out.println(d.getTime()-d1.getTime());
		//assertThat(s2.getDataNasterii()).isEqualToIgnoringMillis(s.getDataNasterii());
		//assertTrue("Is ok",(s2.getDataNasterii().getTime()- s.getDataNasterii().getTime())!=0);
		//assertEquals(d.getDay(),d1.getDay());
		//assertEquals(d.getMonth(),d1.getMonth());
		//assertEquals(d.getYear(),d1.getYear());
		//assertEquals(s2.getId(),s.getId());
		//assertEquals(s2.getNume(), s.getNume());
		//assertEquals(s2.getPrenume(), s.getPrenume());
		//assertEquals(s2.getDataNasterii(), s.getDataNasterii());/*
		//assertEquals(new Student("Cristian", "Cristi", 22, "23-09-1994"),s);
		//assertEquals(s2,s);
	}
	/*
	public void testSelectCurs() {
		CursDAOClass cs=new CursDAOClass();
		Curs c=new Curs();
		c=cs.selectCurs(1);
		Curs c2=new Curs(1, "Fizica", 5);
		System.out.println(c.getName());
		System.out.println(c.getId());
		System.out.println(c.getCredite());
		assertEquals(c2.getId(),c.getId());
	}
	*/

	/*
	public void testDelete() {
		StudentDAOClass st=new StudentDAOClass();
		Student s=new Student();
		s.setNume("Codau");
		s.setPrenume("Iulia");
		s.setDataNasterii(new Date(23/9/1994));
		s.setId(23);
		st.selectStudent(23);
		assertEquals(23, s.getId());
	}*/

}
