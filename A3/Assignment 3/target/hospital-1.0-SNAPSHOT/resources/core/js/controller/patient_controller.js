'use strict';

App.controller('PatientController', ['$scope', 'PatientService', function($scope, PatientService) {
    var self = this;
    self.patient={ id:null,name:'', cnp:'',dateOfBirth:'',address:''};
    self.patients=[];

    self.fetchAllPatients = function(){
        PatientService.fetchAllPatients()
            .then(
                function(d) {
                    self.patients = d;
                },
                function(errResponse){
                    console.error('Error while fetching patients');
                }
            );
    };

    self.createPatient = function(patient){
        PatientService.createPatient(patient)
            .then(
                self.fetchAllPatients,
                function(errResponse){
                    console.error('Error while creating patient.');
                }
            );
    };

    self.updatePatient = function(patient, id){
        PatientService.updatePatient(patient, id)
            .then(
                self.fetchAllPatients,
                function(errResponse){
                    console.error('Error while updating patient.');
                }
            );
    };

    self.deletePatient = function(id){
        PatientService.deletePatient(id)
            .then(
                self.fetchAllPatients,
                function(errResponse){
                    console.error('Error while deleting patient.');
                }
            );
    };

    self.fetchAllPatients();

    self.submit = function() {
        if(self.patient.id===null){
            console.log('Saving New patient', self.patient);
            self.createPatient(self.patient);
        }else{
            self.updatePatient(self.patient, self.patient.id);
            console.log('Patient updated with id ', self.patient.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.patients.length; i++){
            if(self.patients[i].id === id) {
                self.patient = angular.copy(self.patients[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.patient.id === id) {//clean form if the user to be deleted is shown there.
            self.reset();
        }
        self.deletePatient(id);
    };


    self.reset = function(){
        self.patient={ id:null,name:'', cnp:'',dateOfBirth:'',address:''};
        $scope.myForm.$setPristine(); //reset Form
    };

}]);