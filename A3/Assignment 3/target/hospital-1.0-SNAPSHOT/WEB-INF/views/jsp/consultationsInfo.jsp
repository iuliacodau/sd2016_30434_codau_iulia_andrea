<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="myApp">
<head>
    <title>Users stuff bla bla</title>
    <style>
        .username.ng-valid {
            background-color: lightgreen;
        }
        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .username.ng-dirty.ng-invalid-minlength {
            background-color: yellow;
        }

        .email.ng-valid {
            background-color: lightgreen;
        }
        .email.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .email.ng-dirty.ng-invalid-email {
            background-color: yellow;
        }

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body  class="ng-cloak">
<div class="generic-container" ng-controller="ConsultationController as ctrl">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">Consultations Registration Form </span></div>
        <div class="formcontainer">
            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                <input type="hidden" ng-model="ctrl.consultation.id" />

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="patientId">Patient Id</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.consultation.patientId" id="patientId" class="form-control input-sm" placeholder="Enter your patient id."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="doctorId">Doctor Id</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.consultation.doctorId" id="doctorId" class="form-control input-sm" placeholder="Enter your doctor id."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="dateOfConsultation">Date of Consultation</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.consultation.dateOfConsultation" id="dateOfConsultation" class="form-control input-sm" placeholder="Enter the date of consultation."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit"  value="{{!ctrl.consultation.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Consultations </span></div>
        <div class="tablecontainer">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Patient ID</th>
                    <th>Doctor ID</th>
                    <th>Date of Consultation</th>
                    <th width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="consultation in ctrl.consultations">
                    <td><span ng-bind="consultation.id"></span></td>
                    <td><span ng-bind="consultation.patientId"></span></td>
                    <td><span ng-bind="consultation.doctorId"></span></td>
                    <td><span ng-bind="consultation.dateOfConsultation"></span></td>
                    <td>
                        <button type="button" ng-click="ctrl.edit(consultation.id)" class="btn btn-success custom-width">Edit</button>
                        <button type="button" ng-click="ctrl.remove(consultation.id)" class="btn btn-danger custom-width">Remove</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/resources/core/js/app.js' />"></script>
<script src="<c:url value='/resources/core/js/service/consultation_service.js' />"></script>
<script src="<c:url value='/resources/core/js/controller/consultation_controller.js' />"></script>
</body>
</html>