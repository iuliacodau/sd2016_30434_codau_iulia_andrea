import com.hospital.web.dao.PatientDAOImplementation;
import com.hospital.web.model.Patient;
import com.hospital.web.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */
public class MongoTest {

    /*
    public static void main(String[] args) {


        PatientDAOImplementation pdao=new PatientDAOImplementation();

        Patient patient=new Patient();
        patient.setId(12);
        patient.setAddress("Address");
        patient.setCnp("243758432546");
        patient.setDateOfBirth("23-09-1994");
        patient.setName("Iulia");
        pdao.create(patient);
/*
        System.out.println("1. " + pdao.getAllObjects());

        repository.saveObject(new Tree("2", "Orange Tree", 3));

        System.out.println("2. " + repository.getAllObjects());

        System.out.println("Tree with id 1" + repository.getObject("1"));

        repository.updateObject("1", "Peach Tree");

        System.out.println("3. " + repository.getAllObjects());

        repository.deleteObject("2");

        System.out.println("4. " + repository.getAllObjects());
    }
*/

   /* public static void main(String[] args) {

        // For XML
        //ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");

        // For Annotation
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(SpringMongoConfig.class);
        MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

        User user = new User();
        user.setUsername("admin");
        user.setType("ADMIN_ROLE");
        user.setPassword("admin");
        user.setId("1");

        // save
        mongoOperation.save(user);

        // now user object got the created id.
        System.out.println("1. user : " + user);

        // query to search user
        Query searchUserQuery = new Query(Criteria.where("username").is("admin"));

        // find the saved user again.
        User savedUser = mongoOperation.findOne(searchUserQuery, User.class);
        System.out.println("2. find - savedUser : " + savedUser);

        // update password
        mongoOperation.updateFirst(searchUserQuery,
                Update.update("password", "new password"),User.class);

        // find the updated user object
        User updatedUser = mongoOperation.findOne(searchUserQuery, User.class);

        System.out.println("3. updatedUser : " + updatedUser);

        // delete
        mongoOperation.remove(searchUserQuery, User.class);

        // List, it should be empty now.
        List<User> listUser = mongoOperation.findAll(User.class);
        System.out.println("4. Number of user = " + listUser.size());

    }*/
}
