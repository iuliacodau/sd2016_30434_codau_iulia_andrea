'use strict';

App.factory('UserService', ['$http', '$q', function($http, $q){

    return {

        fetchAllUsers: function() {
            return $http.get('http://localhost:8080/rest/users/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching users');
                        return $q.reject(errResponse);
                    }
                );
        },

        createUser: function(user){
            return $http.post('http://localhost:8080/rest/user/', user)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating user');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateUser: function(user, id){
            return $http.put('http://localhost:8080/rest/users/'+id, user)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating user');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateUser2: function(user, id){
            $http({ method: 'PUT', url:'/rest/users/'+id })
                .then (
                    function successCallback() {
                        console.log("Delete successful");
                    },
                    function errorCallback() {
                        console.log("Error at user update");
                    }
                );
        },

        deleteUser2:function(id){
            $http({ method: 'DELETE', url: '/rest/users/'+id })
                .then(
                    function successCallback() {
                        console.log("Delete succesful");
                    },
                    function errorCallback() {
                        console.log("error at user delete");
                    }
                );
        },

        deleteUser: function(id){
            return $http.delete('http://localhost:8080/rest/users/'+id)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting user');
                        return $q.reject(errResponse);
                    }
                );
        }
    };

}]);