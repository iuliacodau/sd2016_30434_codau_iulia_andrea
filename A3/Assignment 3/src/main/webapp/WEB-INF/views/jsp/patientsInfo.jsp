<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="myApp">
<head>
    <title>Patients</title>
    <style>
        .username.ng-valid {
            background-color: lightgreen;
        }
        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .username.ng-dirty.ng-invalid-minlength {
            background-color: yellow;
        }

        .email.ng-valid {
            background-color: lightgreen;
        }
        .email.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .email.ng-dirty.ng-invalid-email {
            background-color: yellow;
        }

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body  class="ng-cloak">
<div class="generic-container" ng-controller="PatientController as ctrl">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">Patient Registration Form </span></div>
        <div class="formcontainer">
            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                <input type="hidden" ng-model="ctrl.patient.id" />


                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="name">Name</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.patient.name" id="name" class="form-control input-sm" placeholder="Enter your name."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="cnp">CNP</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.patient.cnp" id="cnp" class="form-control input-sm" placeholder="Enter your CNP."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="dateOfBirth">Date of Birth</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.patient.dateOfBirth" id="dateOfBirth" class="form-control input-sm" placeholder="Enter your date of birth."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="address">Address</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.patient.address" id="address" class="form-control input-sm" placeholder="Enter your date of address."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit"  value="{{!ctrl.patient.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Patients </span></div>
        <div class="tablecontainer">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>CNP</th>
                    <th>Date of Birth</th>
                    <th>Address</th>
                    <th width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="patient in ctrl.patients">
                    <td><span ng-bind="patient.id"></span></td>
                    <td><span ng-bind="patient.name"></span></td>
                    <td><span ng-bind="patient.cnp"></span></td>
                    <td><span ng-bind="patient.dateOfBirth"></span></td>
                    <td><span ng-bind="patient.address"></span></td>
                    <td>
                        <button type="button" ng-click="ctrl.edit(patient.id)" class="btn btn-success custom-width">Edit</button>
                        <button type="button" ng-click="ctrl.remove(patient.id)" class="btn btn-danger custom-width">Remove</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/resources/core/js/app.js' />"></script>
<script src="<c:url value='/resources/core/js/service/patient_service.js' />"></script>
<script src="<c:url value='/resources/core/js/controller/patient_controller.js' />"></script>
</body>
</html>