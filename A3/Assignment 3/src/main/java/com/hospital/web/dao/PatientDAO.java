package com.hospital.web.dao;


import com.hospital.web.model.Patient;
import com.hospital.web.model.User;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */
public interface PatientDAO {

    public void create(Patient patient);
    public void update(Patient patient);
    public void delete(String id);
    public List<Patient> find();
    public Patient findById(String id);

}
