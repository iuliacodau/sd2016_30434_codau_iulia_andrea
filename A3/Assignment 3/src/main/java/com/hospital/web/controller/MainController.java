package com.hospital.web.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.security.Principal;

/**
 * Created by Iulia on 23.05.2016.
 */

@Controller
@EnableWebMvc
public class MainController {

   /* @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView startPage() {
        ModelAndView modelAndView= new ModelAndView("login");
        return modelAndView;
    }
*/
    @RequestMapping(value = "/{path}", method = RequestMethod.GET)
    public String getPage(@PathVariable("path") String path) {
        return path;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getPathPage() {
        return "login";
    }

/*    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String printWelcome1(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "login";
    }*/

}
