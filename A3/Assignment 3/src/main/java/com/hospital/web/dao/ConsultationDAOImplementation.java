package com.hospital.web.dao;

import com.hospital.web.model.Consultation;
import com.hospital.web.model.Patient;
import com.mongodb.Mongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Repository(value = "CONSULTATION")
public class ConsultationDAOImplementation implements ConsultationDAO {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void create(Consultation consultation) {
        mongoTemplate.insert(consultation);
    }

    @Override
    public void update(Consultation consultation) {
        mongoTemplate.save(consultation);
    }

    @Override
    public void delete(String id) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id)), Consultation.class);
    }

    @Override
    public List<Consultation> findAll() {
        return mongoTemplate.findAll(Consultation.class);
    }

    @Override
    public Consultation findById(String id) {
        Consultation consultation=new Consultation();
        List<Consultation> consultationList=mongoTemplate.find(Query.query(Criteria.where("id").is(id)), Consultation.class);
        if (!consultationList.isEmpty()) {
            consultation=consultationList.get(0);
        }
        return consultation;
    }
}
