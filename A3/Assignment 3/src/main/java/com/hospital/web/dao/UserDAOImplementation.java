package com.hospital.web.dao;

import com.hospital.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Repository(value = "USER")
public class UserDAOImplementation implements UserDAO {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void create(User user) {
        mongoTemplate.insert(user);
    }

    @Override
    public void update(User user) {
        mongoTemplate.save(user);
    }

    @Override
    public void delete(String id) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id)), User.class);
    }

    @Override
    public List<User> find() {
        return mongoTemplate.findAll(User.class);
    }

    @Override
    public User findByUsername(String username) {
        User user=new User();
        List<User> userList=mongoTemplate.find(Query.query(Criteria.where("username").is(username)), User.class);
        if(!userList.isEmpty()){
            user=userList.get(0);
        }
        return user;
    }

    @Override
    public User findById(String id) {
        User user=new User();
        List<User> userList=mongoTemplate.find(Query.query(Criteria.where("id").is(id)), User.class);
        if (!userList.isEmpty()) {
            user=userList.get(0);
        }
        return user;
    }
}
