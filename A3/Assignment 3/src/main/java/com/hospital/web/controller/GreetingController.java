package com.hospital.web.controller;

import com.hospital.web.model.Greeting;
import com.hospital.web.model.Message;
import org.springframework.stereotype.Controller;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;

/**
 * Created by Iulia on 25.05.2016.
 */

@Controller
public class GreetingController {

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(Message message) throws Exception {
        return new Greeting ("New patient : " + message.getName() +"!!");
    }
}
