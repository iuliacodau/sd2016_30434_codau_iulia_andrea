package com.hospital.web.controller;

import com.hospital.web.dao.ConsultationDAO;
import com.hospital.web.model.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import sun.misc.Contended;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class ConsultationController {
    @Autowired
    ConsultationDAO consultationDAO;

    //Create a new consultation
    @RequestMapping(value = "/consultation", method = RequestMethod.POST, consumes = "application/JSON", produces = "application/JSON" )
    @ResponseBody
    public ResponseEntity<Consultation> createNewConsultation(@RequestBody Consultation consultation) {
        try {
            consultationDAO.create(consultation);
            System.out.println("HERE");
        } catch (Exception e) {
            return new ResponseEntity<Consultation>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders hdr=new HttpHeaders();
        hdr.add("Location", "/rest/consultations/" + consultation.getId());
        return new ResponseEntity<Consultation>(consultation, hdr, HttpStatus.CREATED);
    }

    //Select consultation according to its id (in order to update or delete later)
    @RequestMapping(value = "/consultations/{id}", method = RequestMethod.GET, produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Consultation> selectConsultation(@PathVariable String id) {
        try {
            Consultation consultation = consultationDAO.findById(id);
            return new ResponseEntity<Consultation>(consultation, HttpStatus.FOUND);
        } catch(Exception e) {
            return new ResponseEntity<Consultation>(HttpStatus.NOT_FOUND);
        }
    }

    //Get all the users from the database
    @RequestMapping (value = "/consultations", method = RequestMethod.GET, produces="application/JSON")
    @ResponseBody
    public ResponseEntity<List<Consultation>> showConsultations(Model model) {
        List<Consultation> consultationList;
        try {
            model.addAttribute("consultation", new Consultation());
            model.addAttribute("listConsultations", consultationDAO.findAll());
            consultationList=consultationDAO.findAll();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } return new ResponseEntity<>(consultationList, HttpStatus.OK);
    }

    //Updating a user
    @RequestMapping (value = "/consultations/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Consultation> updateConsultation(@PathVariable("id") String id, @RequestBody Consultation consultation) {
        try {
            if (consultationDAO.findById(id).getId().equals(id)) {
                consultation.setId(id);
                consultationDAO.update(consultation);
            }
        } catch (Exception e) {
            return new ResponseEntity<Consultation>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Consultation>(consultation, HttpStatus.OK);
    }

    //Deleting a user
    @RequestMapping (value = "/consultations/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Consultation> deleteConsultation(@PathVariable("id") String id) {
        try {
            consultationDAO.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<Consultation>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Consultation>(HttpStatus.OK);
    }

}
