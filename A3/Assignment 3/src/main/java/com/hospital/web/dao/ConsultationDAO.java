package com.hospital.web.dao;

import com.hospital.web.model.Consultation;
import com.hospital.web.model.Patient;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */
public interface ConsultationDAO {

    public void create(Consultation consultation);
    public void update(Consultation consultation);
    public void delete(String id);
    public List<Consultation> findAll();
    public Consultation findById(String id);
}
