package com.hospital.web.controller;

import com.hospital.web.dao.PatientDAO;
import com.hospital.web.model.Patient;
import com.hospital.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class PatientController {

    @Autowired
    PatientDAO patientDAO;

    //Creating a new patient
    @RequestMapping(value = "/patient", method = RequestMethod.POST, consumes = "application/JSON", produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Patient> createNewPatient(@RequestBody Patient patient) {
        try {
            patientDAO.create(patient);
        } catch (Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders hdr=new HttpHeaders();
        hdr.add("Location", "/rest/patients/" + patient.getId());
        return new ResponseEntity<Patient>(patient, hdr, HttpStatus.CREATED);
    }

    //Select patient according to its id (in order to update or delete later
    @RequestMapping(value = "/patients/{id}", method = RequestMethod.GET, produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Patient> selectPatient(@PathVariable String id) {
        try {
            Patient patient = patientDAO.findById(id);
            return new ResponseEntity<Patient>(patient, HttpStatus.FOUND);
        } catch(Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
        }
    }

    //Get all the patients from the database
    @RequestMapping (value = "/patients", method = RequestMethod.GET, produces="application/JSON")
    @ResponseBody
    public ResponseEntity<List<Patient>> showPatients(Model model) {
        List<Patient> patientList;
        try {
            model.addAttribute("patient", new Patient());
            model.addAttribute("listPatients", patientDAO.find());
            patientList=patientDAO.find();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } return new ResponseEntity<>(patientList, HttpStatus.OK);
    }

    //Updating a patient
    @RequestMapping (value = "/patients/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Patient> updatePatient(@PathVariable("id") String id, @RequestBody Patient patient) {
        try {
            // if (userDAO.findById(id).getUsername().equals(user.getUsername())) {
            if (patientDAO.findById(id).getId().equals(id)) {
                patient.setId(id);
                patientDAO.update(patient);
            }
        } catch (Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Patient>(patient, HttpStatus.OK);
    }

    //Deleting a patient
    @RequestMapping (value = "/patients/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Patient> deletePatient(@PathVariable("id") String id) {
        try {
            patientDAO.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Patient>(HttpStatus.OK);
    }

}
