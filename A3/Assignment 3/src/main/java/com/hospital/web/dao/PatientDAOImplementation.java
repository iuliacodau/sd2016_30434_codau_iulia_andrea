package com.hospital.web.dao;

import com.hospital.web.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Repository(value="PATIENT")
public class PatientDAOImplementation implements PatientDAO{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void create(Patient patient) {
        mongoTemplate.insert(patient);
    }

    @Override
    public void update(Patient patient) {
        mongoTemplate.save(patient);
    }

    @Override
    public void delete(String id) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id)), Patient.class);
    }

    @Override
    public List<Patient> find() {
        return mongoTemplate.findAll(Patient.class);
    }

    @Override
    public Patient findById(String id) {
        Patient patient=new Patient();
        List<Patient> patientList=mongoTemplate.find(Query.query(Criteria.where("id").is(id)), Patient.class);
        if (!patientList.isEmpty()) {
            patient=patientList.get(0);
        }
        return patient;
    }
}
