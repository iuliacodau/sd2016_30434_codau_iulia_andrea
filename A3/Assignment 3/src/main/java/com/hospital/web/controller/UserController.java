package com.hospital.web.controller;

import com.hospital.web.dao.UserDAO;
import com.hospital.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class UserController {
    @Autowired
    UserDAO userDAO;

    /*@RequestMapping(value="/welcomeAdmin", method = RequestMethod.GET)
    public String printWelcomeAdmin(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeAdmin";
    }

    @RequestMapping(value="/welcomeDoctor")
    public String printWelcomeDoctor(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeDoctor";
    }

    @RequestMapping(value="/welcomeSecretary")
    public String printWelcomeSecretary(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeSecretary";
    }*/

/*    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }*/

    //Creating a new user
    @RequestMapping(value = "/user", method = RequestMethod.POST, consumes = "application/JSON", produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> createNewUser(@RequestBody User user) {
        try {
            userDAO.create(user);
            System.out.println("I got the data "+user.toString());
        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders hdr=new HttpHeaders();
        hdr.add("Location", "/rest/users/" + user.getId());
        return new ResponseEntity<User>(user, hdr, HttpStatus.CREATED);
    }

    //Select user according to its id (in order to update or delete later
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> selectUser(@PathVariable String id) {
        try {
            User user = userDAO.findById(id);
            System.out.println("I am in the get user");
            return new ResponseEntity<User>(user, HttpStatus.FOUND);
        } catch(Exception e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    //Get all the users from the database
    @RequestMapping (value = "/users", method = RequestMethod.GET, produces="application/JSON")
    @ResponseBody
    public ResponseEntity<List<User>> showUsers(Model model) {
        List<User> usersList;
        try {
            model.addAttribute("user", new User());
            model.addAttribute("usersList", userDAO.find());
            usersList=userDAO.find();
            System.out.println("I am in the get all users");
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } return new ResponseEntity<>(usersList, HttpStatus.OK);
    }

    //Updating a user
    @RequestMapping (value = "/users/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<User> updateUser(@PathVariable("id") String id, @RequestBody User user) {
        try {
            System.out.println("What what");
           // if (userDAO.findById(id).getUsername().equals(user.getUsername())) {
            if (userDAO.findById(id).getId().equals(id)) {
                user.setId(id);
                userDAO.update(user);
            }
            } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    //Deleting a user
    @RequestMapping (value = "/users/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<User> deleteUser(@PathVariable("id") String id) {
        try {
            System.out.println("Why not");
                userDAO.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(HttpStatus.OK);
    }

}
