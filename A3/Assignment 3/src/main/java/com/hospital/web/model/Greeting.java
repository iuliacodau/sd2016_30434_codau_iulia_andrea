package com.hospital.web.model;

/**
 * Created by Iulia on 25.05.2016.
 */
public class Greeting {

    private String content;

    public Greeting(String content) {
        this.content=content;
    }

    public String getContent() {
        return content;
    }
}
