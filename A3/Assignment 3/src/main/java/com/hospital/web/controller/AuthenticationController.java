package com.hospital.web.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * Created by Iulia on 24.05.2016.
 */

@Controller
@EnableWebMvc
//@RequestMapping("/rest")
public class AuthenticationController {

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String printWelcome1(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "login";
    }

    @RequestMapping(value="/welcomeAdmin", method = RequestMethod.GET)
    public String printWelcomeAdmin(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeAdmin";
    }

    @RequestMapping(value="/welcomeDoctor")
    public String printWelcomeDoctor(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeDoctor";
    }

    @RequestMapping(value="/welcomeSecretary")
    public String printWelcomeSecretary(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeSecretary";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }
}
