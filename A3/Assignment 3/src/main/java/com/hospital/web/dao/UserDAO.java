package com.hospital.web.dao;

import com.hospital.web.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Iulia on 23.05.2016.
 */

@Component
public interface UserDAO {

    public void create(User user);
    public void update(User user);
    public void delete(String id);
    public List<User> find();
    public User findByUsername(String username);
    public User findById(String id);

}
