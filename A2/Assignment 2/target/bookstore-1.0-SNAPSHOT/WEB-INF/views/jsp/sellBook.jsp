<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 09.05.2016
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Iulia's Bookstore</title>


    <spring:url value="/resources/core/css/stylish-portfolio.css" var="coreCss" />
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>

<c:if test="${book.id != null}">
    <div class="col-lg-10 col-lg-offset-1 text-center">
        <h3>Sell book!</h3>
    </div>
</c:if>

<div class="container">

    <div class="row">

        <div class="col-lg-10 col-lg-offset-1 text-center">
            <form:form class="form-horizontal" commandName="book" method="POST" action="${cp}/createSelling">

                <div class="form-group ">
                    <label for="bookId" class="col-sm-2 control-label">ID</label>
                    <form:input path="id" id="bookId"></form:input>
                </div>


                <div class="form-group ">
                    <label for="titleId" class="col-sm-2 control-label">Title</label>
                    <div class="required col-sm-10">
                        <form:input path="title" id="titleId" ></form:input>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="authorId" class="col-sm-2 control-label">Author</label>
                    <div class="required col-sm-10">
                        <form:input path="author" id="authorId" ></form:input>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="genreId" class="col-sm-2 control-label">Genre</label>
                    <div class="required col-sm-10">
                        <form:input path="genre" id="genreId" ></form:input>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="quantityId" class="col-sm-2 control-label">Quantity</label>
                    <form:input path="quantity" id="quantityId"></form:input>
                </div>

                <div class="form-group ">
                    <label for="priceId" class="col-sm-2 control-label">Price</label>
                    <form:input path="price" id="priceId"></form:input>
                </div>



                <div class="form-group ">
                    <div class="col-sm-offset-1 col-sm-10 text-center">
                        <input type="submit" name="submit" value="OK" />
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</div>







<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <hr class="small">
                <p class="text-muted">Copyright &copy; Iulia's Bookstore</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="/resources/core/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/core/js/bootstrap.min.js"></script>

</body>
</html>