<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 09.05.2016
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Iulia's Bookstore</title>


    <spring:url value="/resources/core/css/stylish-portfolio.css" var="coreCss" />
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">


    <script>
        function validateForm() {
            var username = document.forms["form1"]["username"].value;
            var password= document.forms["form1"]["password"].value;
            var name = document.forms["form1"]["name"].value;
            var age= document.forms["form1"]["age"].value;
            var type = document.forms["form1"]["type"].value;
            if (username == null || username == "") {
                alert("Username must be filled out");
                return false;
            }
            if (password == null || password == "" || password.length<8) {
                alert("Password must be filled out and must have 8 characters");
                return false;
            }
            if (name == null || name == "") {
                alert("Name must be filled out");
                return false;
            }
            if (age <18 ) {
                alert("Age must be greater than 18");
                return false;
            }
            if (type != "ADMIN_ROLE" && type !="USER_ROLE" && !type.equals("ADMIN_ROLE") && !type.equals("USER_ROLE")) {
                alert("Type must be either ADMIN_ROLE or USER_ROLE but it is "+ type+" instead");
                return false;
            }
        }
    </script>

</head>
<body>


<c:if test="${user.id == null}">
<div class="col-lg-10 col-lg-offset-1 text-center">
    <h3>Create!</h3>
    </div>
</c:if>
<c:if test="${user.id != null}">
<div class="col-lg-10 col-lg-offset-1 text-center">
    <h3>Edit!</h3>
    </div>
</c:if>

<div class="container">

    <div class="row">

    <div class="col-lg-10 col-lg-offset-1 text-center">
<form:form class="form-horizontal" name="form1" commandName="user" method="POST" action="${cp}/update" onsubmit="return validateForm()">



    <div class="form-group ">
    <label for="usernameId" class="col-sm-2 control-label">Username</label>
        <div class="required col-sm-10">
    <form:input path="username" id="usernameId" ></form:input>
            </div>
    </div>

    <div class="form-group ">
    <label for="passwordId" class="col-sm-2 control-label">Password</label>
        <div class="required col-sm-10">
    <form:input path="password" id="passwordId"></form:input>
            </div>
    </div>

    <div class="form-group ">
    <label for="nameId" class="col-sm-2 control-label">Name</label>
        <div class="required col-sm-10">
    <form:input path="name" id="nameId"></form:input>
    </div>
        </div>

    <div class="form-group ">
    <label for="ageId" class="col-sm-2 control-label">Age</label>
    <form:input path="age" id="ageId"></form:input>

    </div>


    <div class="form-group ">
        <label for="typeId" class="col-sm-2 control-label">Type</label>
        <div class="required col-sm-10">
            <form:input path="type" id="typeId"></form:input>
        </div>
    </div>


    <div class="form-group ">
        <div class="col-sm-offset-1 col-sm-10 text-center">
    <input type="submit" name="submit" value="OK" />
            </div>
    </div>

</form:form>
    </div>
    </div>
</div>







<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <hr class="small">
                <p class="text-muted">Copyright &copy; Iulia's Bookstore</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="/resources/core/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/core/js/bootstrap.min.js"></script>

</body>
</html>