<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″>
    <title>JSP Page</title>
</head>
<body>
<c:if test="${user.id == null}">
<h3>Create!</h3>
</c:if>
<c:if test="${user.id != null}">
<h3>Edit!</h3>
</c:if>
<form:form commandName="user" method="POST" action="{cp}/edit">
<form:hidden path="id" />

<label for="nameId">Name</label>
<form:input path="name" id="nameId" ></form:input>

<label for="ageId">Age</label>
<form:input path="age" id="ageId"></form:input>

<input type=”submit” name=”submit” value=”OK” />
</form:form>
</body>
</html>
