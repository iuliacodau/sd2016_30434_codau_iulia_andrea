<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Iulia's Bookstore</title>

    <spring:url value="/resources/core/css/stylish-portfolio.css" var="coreCss" />
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>


<!-- Header -->
<header id="top" class="header3">
    <div class="text-vertical-center">
        <h1>Iulia's Bookstore</h1>
        <h3>Search for the best books &amp; buy them on spot</h3>
        <br>
        <a href="${pageContext.request.contextPath}/login" class="btn btn-dark btn-lg">Login</a>
    </div>
</header>

</body>
</html>