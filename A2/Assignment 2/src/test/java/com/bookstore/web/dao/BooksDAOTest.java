package com.bookstore.web.dao;

import org.junit.Test;

import javax.xml.bind.JAXBException;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Iulia on 19.05.2016.
 */
public class BooksDAOTest {

    private BooksDAO booksDAO=new BooksDAO();

    public BooksDAOTest() throws JAXBException {
    }

    @Test
    public void testReader() throws JAXBException {
        // The reader is initialized and bound to the input data
        booksDAO.booksOnStock();
        System.out.println("Book " + booksDAO.getBooks().getBooks().get(1));
        assertNotNull(booksDAO.booksOnStock());
        assertNotNull(booksDAO.selectBooksByAuthor("Harper Lee"));

    }
}
