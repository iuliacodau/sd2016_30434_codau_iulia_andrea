<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 15.05.2016
  Time: 22:32
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h1>
    Search Books
</h1>

<div class="container">
    <form:form action="/submitSearchForm" method="POST" commandName="book">
        <table>
            <tr>
                <td>Genre</td>
                <td><input path="genre"/></td>
            </tr>
            <tr>
                <td>Title</td>
                <td><input type="text" path="title"/></td>
            </tr>
            <tr>
                <td>Author</td>
                <td><input path="author"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="action" value="search" />
                </td>
            </tr>
        </table>
    </form:form>>
    <br>
    <table>
        <tr> Book Title</tr>
        <c:forEach items="${booksSearchedList}" var="book">
            <tr>
                <td>${book.title}</td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
