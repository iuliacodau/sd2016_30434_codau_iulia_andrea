<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 09.05.2016
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Iulia's Bookstore</title>


    <spring:url value="/resources/core/css/stylish-portfolio.css" var="coreCss" />
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <script>
        function validateForm() {
            var customerName = document.forms["form1"]["customerName"].value;
            var quantity= document.forms["form1"]["quantity"].value;
            if (customerName == null || customerName == "") {
                alert("Customer Name must be filled out");
                return false;
            }
            if (quantity <0 || quantity>${book.quantity}) {
                alert("Not enough books in stock");
                return false;
            }
        }
    </script>

</head>
<body>


<c:if test="${book.id == null}">
    <div class="col-lg-10 col-lg-offset-1 text-center">
        <h3>Create!</h3>
    </div>
</c:if>
<c:if test="${book.id != null}">
    <div class="col-lg-10 col-lg-offset-1 text-center">
        <h3>Sell Book!</h3>
    </div>
</c:if>

<div class="container">

    <div class="row">


        <div class="col-md-9 col-md-offset-1">
            <table class="table table-list-search">

                <!--<table class="table">-->
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Genre</th>
                </tr>
                <tr>
                    <td>${book.id}</td>
                    <td>${book.title}</td>
                    <td>${book.author}</td>
                    <td>${book.price}</td>
                    <td>${book.quantity}</td>
                    <td>${book.genre}</td>
                </tr>
            </table>
        </div>
        </div>


    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 text-center">
            <form:form class="form-horizontal" name="form1" commandName="selling" method="POST" action="${cp}/updateBookSell" onsubmit="return validateForm()">


                <div class="form-group ">
                    <label for="quantityId" class="col-sm-2 control-label">Quantity </label>
                    <form:input path="quantity" id="quantityId" ></form:input>
                </div>


                <div class="form-group ">
                    <label for="customerNameId" class="col-sm-2 control-label">Customer Name</label>
                    <div class="required col-sm-10">
                        <form:input path="customerName" id="customerNameId" ></form:input>
                    </div>
                </div>


                <div class="form-group ">
                    <div class="col-sm-offset-1 col-sm-10 text-center">
                        <input type="submit" name="submit" value="OK" />
                    </div>
                </div>

            </form:form>




        </div>
    </div>
</div>







<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <hr class="small">
                <p class="text-muted">Copyright &copy; Iulia's Bookstore</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="/resources/core/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/core/js/bootstrap.min.js"></script>

</body>
</html>