<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Iulia's Bookstore</title>
    <spring:url value="/resources/core/css/stylish-portfolio.css" var="coreCss" />
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>

<!-- Callout -->
<aside class="callout2">
    <div class="text-vertical-center">
        <h1>Enter account</h1>
    </div>
</aside>

<!-- Portfolio-->
<section id="portfolio" class="portfolio">
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1 text-center">
            <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
                <font color="red">
                    Your login attempt was not successful due to <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
                </font>
            </c:if>
            <form class="form-horizontal" action="j_spring_security_check" method='POST'>
                <div class="form-group ">
                    <label for="inputName3" class="col-sm-2 control-label">Username</label>
                    <div class="required col-sm-10">
                        <input type="text" name="j_username" class="form-control" id="inputName3" placeholder="Text Input">
                    </div>
                </div>
                <div class="required form-group ">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="j_password" class="form-control" id="inputPassword3" placeholder="Password">
                        <input type="hidden"
                               name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-10 text-center">
                        <button type="submit" class="btn btn-lg btn-dark">Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>


<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <hr class="small">
                <p class="text-muted">Copyright &copy; Iulia's Bookstore</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="/resources/core/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/core/js/bootstrap.min.js"></script>

</body>
</html>