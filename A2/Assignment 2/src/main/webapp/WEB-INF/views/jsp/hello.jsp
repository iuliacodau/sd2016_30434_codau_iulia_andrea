<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Iulia's Bookstore</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
	<div class="navbar-header">
		<a class="navbar-brand" href="${pageContext.request.contextPath}/index">Iulia's Bookstore</a>
		<a class="navbar-brand" href="${pageContext.request.contextPath}/login">Login</a>
		<a class="navbar-brand" href="${pageContext.request.contextPath}/403">ReadMe</a>
	</div>
  </div>
</nav>




 
<div class="jumbotron">
  <div class="container">
	<h1>${title}</h1>
	<p>
		<c:if test="${not empty name}">
			Hello ${name}
		</c:if>
 
		<c:if test="${empty name}">
			Welcome Welcome!
		</c:if>
    </p>
	  <!--
    <p>
		<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
	</p>
	-->
	</div>
</div>
 
<div class="container">
 
  <div class="row">
	<div class="col-md-4">
		<h2>Books</h2>
		<p>
			<a class="btn btn-default" href="#" role="button">See all books</a>
		</p>
	</div>
	<div class="col-md-4">
		<h2>Users</h2>
		<p>
			<a class="btn btn-default" href="#" role="button">See all users</a>
		</p>
	</div>
	<div class="col-md-4">
		<h2>Orders</h2>
		<p>
			<a class="btn btn-default" href="#" role="button">See all orders</a>
		</p>
	</div>
  </div>

 
 
  <hr>
  <footer>
	<p>&copy; Iulia's Bookstore</p>
  </footer>
</div>
 
<spring:url value="/resources/core/css/hello.js" var="coreJs" />
<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 
</body>
</html>