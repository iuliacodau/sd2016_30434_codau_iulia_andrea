<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Delete User</title>
</head>
<body>
<h3>Delete!</h3>
<form:form commandName="user" method="POST" action="${cp}/delete">
<p>
    Are you sure you want to delete <strong>${user.name}</strong>?
</p>
<form:hidden path="id" />
<input type="submit" name="submit" value="Delete" />
</form:form>
</body>
</html>