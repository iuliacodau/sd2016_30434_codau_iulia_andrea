package com.bookstore.web.controller;

import com.bookstore.web.model.Book;
import com.bookstore.web.model.Report;
import com.bookstore.web.model.ReportFactory;
import com.bookstore.web.model.Selling;
import com.bookstore.web.service.BookService;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Iulia on 10.05.2016.
 */

@Controller
public class BookController {

    @Autowired
    private BookService bookService;

    //Select all books

    @RequestMapping(value = "/selectBooks", method = RequestMethod.GET)
    public String selectBooks(ModelMap map) throws JAXBException {
        map.put("bookList", bookService.listOfBooks());
        return "/selectBooks";
    }

    @RequestMapping(value = "/selectBooksUser", method = RequestMethod.GET)
    public String selectBooksUser(ModelMap map) throws JAXBException {
        map.put("bookList", bookService.listOfBooks());
        return "/selectBooksUser";
    }

    //update book form
    @RequestMapping(value = "/updateBook/{id}", method = RequestMethod.GET)
    public String updateBook(@PathVariable(value="id") int id, ModelMap map, HttpServletRequest request) throws JAXBException {
        map.put("book", bookService.getBook(id));
        map.put("cp", request.getContextPath());
        return "/updateBook";
    }

    //update book
    @RequestMapping(value = "/updateBook", method = RequestMethod.POST)
    public String updateBook(@ModelAttribute("book")@Valid Book book, ModelMap map) throws JAXBException {
        bookService.updateBook(book.getId(), book.getTitle(), book.getAuthor(), book.getGenre(), book.getQuantity(), book.getPrice() );
        //redirect to prevent refresh button or back button posting the form again
        return "redirect:/selectBooks";
    }

    @RequestMapping(value = "/sellBooks", method = RequestMethod.GET)
    public String sellBooks(ModelMap map) throws JAXBException {
        map.put("bookList", bookService.listOfBooks());
        return "/sellBooks";
    }

    //update book form
    @RequestMapping(value = "/updateBookSell/{id}", method = RequestMethod.GET)
    public String updateBookSell(@PathVariable(value="id") int id, ModelMap map, HttpServletRequest request) throws JAXBException {
        map.put("book", bookService.getBook(id));
        map.put("cp", request.getContextPath());
        map.put("selling", new Selling());
        System.out.println("Deleting for "+id);
        return "/updateBookSell";
    }

    //update book
    @RequestMapping(value = "/updateBookSell", method = RequestMethod.POST)
    public String updateBookSell(@ModelAttribute("book")@Valid Book book,ModelMap map) throws JAXBException {
        int updated=book.getQuantity()-2;
        System.out.println(updated);
        bookService.updateBook(book.getId(), book.getTitle(), book.getAuthor(), book.getGenre(), updated, book.getPrice() );
        //redirect to prevent refresh button or back button posting the form again
        System.out.println("Are you here?");
        return "redirect:/sellBooks";
    }

    //create
    @RequestMapping(value = "/createBook", method = RequestMethod.GET)
    public String createBook(ModelMap map, HttpServletRequest request) {
        map.put("cp", request.getContextPath());
        map.put("book", new Book());
        return "updateBook";
    }

    @RequestMapping("/deleteBook/{id}")
    public String deleteNew(@PathVariable int id, RedirectAttributes redirectAttr) throws JAXBException {
        bookService.deleteBook(id);
        System.out.println("Here");
        redirectAttr.addFlashAttribute("message", "Book was deleted");
        return "redirect:/selectBooks";
    }


    @RequestMapping(value = "/createSelling", method = RequestMethod.GET)
    public String createSelling(ModelMap map, HttpServletRequest request) {
        map.put("cp", request.getContextPath());
        map.put("selling", new Selling());
        return "createSelling";
    }

    //search
/*
    @RequestMapping(value = "/search")
    public String Search(@RequestParam("searchString") String searchString) throws JAXBException {
        System.out.println("The genre is "+ searchString);
        if(searchString != null){
            bookService.listOfBooksbyGenre(searchString);
        }
        return "search";
    }*/


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String Search(@RequestParam("searchString") String searchString, ModelMap map) throws JAXBException {
        System.out.println("The genre is "+ searchString);
        if(searchString != null){
            if (bookService.listOfBooksbyGenre(searchString) == null) {
                if (bookService.listOfBooksbyTitle(searchString) == null) {
                    if (bookService.listOfBooksbyAuthor(searchString) != null) {
                        bookService.listOfBooksbyAuthor(searchString);
                        System.out.println("Here");
                        map.put("bookList", bookService.listOfBooksbyAuthor(searchString));
                    }
                } else {
                    bookService.listOfBooksbyTitle(searchString);
                    map.put("bookList", bookService.listOfBooksbyTitle(searchString));
                }
            } else {
                bookService.listOfBooksbyGenre(searchString);
                map.put("bookList", bookService.listOfBooksbyGenre(searchString));
            }
        }
       // map.put("bookList", bookService.listOfBooksbyGenre(searchString));
        return "/selectBooksUser";
    }
/*
    @RequestMapping(value = "/search/{genre}", method = RequestMethod.GET)
    public String getBookByGenre(@PathVariable("genre")String genre, ModelMap map) throws JAXBException {
        System.out.println("The GENRE: "+ genre);
        map.put("bookList", bookService.listOfBooksbyGenre(genre));
        return "/selectBooks";
    }*/

    @RequestMapping(value="/searchForm", method = RequestMethod.GET)
    public ModelAndView getSearchForm() {
        ModelAndView model=new ModelAndView("SearchForm");
        return model;
    }

    @RequestMapping(value = "/submitSearchForm", method = RequestMethod.POST)
    public ModelAndView submitSearchForm(@RequestParam("genre")String genre) {
        ModelAndView model=new ModelAndView("SearchSuccess");
        model.addObject("msg", "Details " + genre);
        return model;
    }
    //reports

    @RequestMapping(value="/books", method=RequestMethod.POST)
    public String searchForIt(@ModelAttribute Book book, @RequestParam String action, Map<String, Object> map) throws JAXBException {
        Book b=new Book();
        bookService.listOfBooksbyGenre(book.getGenre());
        map.put("booksSearchedList",bookService.listOfBooksbyGenre(book.getGenre()));
        return "books";
    }

    @RequestMapping(value = "/reports/pdf", method = RequestMethod.GET)
    public void doGetPdf(HttpServletRequest request, HttpServletResponse response) throws IOException, JAXBException, DocumentException {
        ReportFactory reportFactory=new ReportFactory();
        Report report=reportFactory.getReport("PDF");
        report.generate();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "attachement; filename=SalesReport.pdf" );
        ServletOutputStream sos;
        sos=response.getOutputStream();
        File file = new File("E:\\sd2016_A2\\src\\main\\resources\\SalesReport.pdf");
        ByteArrayOutputStream baos = getByteArrayOutputStream(file);
        response.setContentLength(baos.size());
        baos.writeTo(sos);
        sos.flush();
    }

    @RequestMapping(value = "/reports/csv", method = RequestMethod.GET)
    public void doGetCsv(HttpServletRequest request, HttpServletResponse response) throws IOException, JAXBException, DocumentException {
        ReportFactory reportFactory=new ReportFactory();
        Report report=reportFactory.getReport("CSV");
        report.generate();
        response.setContentType("text/csv");
        response.setHeader("Content-disposition", "attachement; filename=SalesReport.csv" );
        ServletOutputStream sos;
        sos=response.getOutputStream();
        File file = new File("E:\\sd2016_A2\\src\\main\\resources\\SalesReport.csv");
        ByteArrayOutputStream baos = getByteArrayOutputStream(file);
        response.setContentLength(baos.size());
        baos.writeTo(sos);
        sos.flush();
    }

    private ByteArrayOutputStream getByteArrayOutputStream(File file) throws IOException {
        //File file = new File("E:\\sd2016_A2\\src\\main\\resources\\SalesReport.pdf");
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[256];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
                //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
                System.out.println("read " + readNum + " bytes,");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return bos;
    }



}
