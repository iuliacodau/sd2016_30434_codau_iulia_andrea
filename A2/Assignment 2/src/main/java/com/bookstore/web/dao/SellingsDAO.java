package com.bookstore.web.dao;

import com.bookstore.web.model.Selling;
import com.bookstore.web.model.Sellings;
import org.springframework.stereotype.Repository;

import javax.xml.bind.*;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Iulia on 08.05.2016.
 */

@Repository("sellingsDAO")
public class SellingsDAO implements SellingsDAOInterface {

    Sellings sellings=new Sellings();
    JAXBContext jaxbContext=JAXBContext.newInstance(Sellings.class);
    Marshaller jaxbMarshaller=jaxbContext.createMarshaller();
    Unmarshaller jaxbUnmarshaller=jaxbContext.createUnmarshaller();

    public SellingsDAO() throws JAXBException {
    }

    @Override
    public void createSelling(int id, int bookId, int employeeId, int quantity, double price, String customerName) throws JAXBException {
        sellings.setSellings(new ArrayList<Selling>());
        Selling s=new Selling();
        s.setPrice(price);
        s.setBookId(bookId);
        s.setCustomerName(customerName);
        s.setEmployeeId(employeeId);
        s.setId(id);
        s.setQuantity(quantity);
        sellings.getSellings().add(s);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(sellings, new File("E:\\sd2016_A2\\src\\main\\resources\\Sellings.xml"));
    }
}
