package com.bookstore.web.service;

import com.bookstore.web.model.Book;


import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by Iulia on 10.05.2016.
 */
public interface BookService {

    public List<Book> listOfBooks() throws JAXBException;
    public Book getBook(int id) throws JAXBException;
    public void updateBook(int id, String title, String author, String genre, int quantity, double price) throws JAXBException;
    public void deleteBook(int id) throws JAXBException;
    public List<Book> listOfBooksbyGenre(String genre) throws JAXBException;
    public List<Book> listOfBooksbyTitle(String title) throws JAXBException;
    public List<Book> listOfBooksbyAuthor(String author) throws JAXBException;
}
