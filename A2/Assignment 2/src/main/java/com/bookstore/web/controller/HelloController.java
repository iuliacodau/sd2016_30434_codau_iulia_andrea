package com.bookstore.web.controller;

import java.security.Principal;

import com.bookstore.web.dao.BooksDAO;
import com.bookstore.web.dao.UsersDAO;
import com.bookstore.web.model.Book;
import com.bookstore.web.model.User;
import com.bookstore.web.service.BookService;
import com.bookstore.web.service.UserService;
import com.bookstore.web.service.UserServiceClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SystemWideSaltSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Iulia on 02.05.2016.
 */

@Controller
public class HelloController {

    @RequestMapping (value="/", method = RequestMethod.GET)
    public ModelAndView startPage() {
        ModelAndView modelandview=new ModelAndView("landingPage");
        return modelandview;
    }

    @RequestMapping(value="/index")
    public String indexPage(ModelMap map) {
        return "index";
    }

    @RequestMapping(value="/403Page")
    public String errorPage(ModelMap map) {
        return "403Page";
    }

    @RequestMapping(value="/welcome", method = RequestMethod.GET)
    public String printWelcome(ModelMap model, Principal principal ) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
        model.addAttribute("username", name);
        return "welcome";
    }

    @RequestMapping(value="/welcomeUser")
    public String printWelcomeUser(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        return "welcomeUser";
    }

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String printWelcome1(ModelMap model, Principal principal ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        System.out.println("whay "+name);
        model.addAttribute("username", name);
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/landingPage";
    }

    @RequestMapping(value="/landingPage")
    public ModelAndView landingPage() {
      return new ModelAndView("landingPage");
    }

}
