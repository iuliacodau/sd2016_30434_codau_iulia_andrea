package com.bookstore.web.service;

import com.bookstore.web.dao.UsersDAO;
import com.bookstore.web.model.User;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Iulia on 09.05.2016.
 */

@Service("userService")
public class UserServiceClass implements UserService {

    @Autowired
    private UsersDAO usersDAO=new UsersDAO();

    public UserServiceClass() throws JAXBException, IOException {

    }

    @Override
    public List<User> listOfUsers() throws JAXBException {
        return usersDAO.selectAllUsers();
    }

    @Override
    public User getUser(int id) throws JAXBException {
        return usersDAO.selectUser(id);
    }

    public User getUserByUsername(String username) throws JAXBException {
        return usersDAO.selectUserByUsername(username);
    }

    @Override
    public void updateUser(int id, String username, String password, String name, int age, String type) throws JAXBException {
        if (id!=0)
            usersDAO.updateUser(id, username, password, name, age, type);
        else usersDAO.createUser(id, username, password, name, age, type);
    }

    @Override
    public void deleteUser(int id) throws JAXBException {
        usersDAO.deleteUser(id);
    }

    public boolean matchingCredentials(String username, String password) throws JAXBException {
        return usersDAO.matchingCredentials(username, password);
    }
}
