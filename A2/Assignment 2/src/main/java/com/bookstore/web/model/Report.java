package com.bookstore.web.model;

import com.itextpdf.text.DocumentException;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Iulia on 10.05.2016.
 */
public interface Report {
    void generate() throws IOException, DocumentException, JAXBException;
}
