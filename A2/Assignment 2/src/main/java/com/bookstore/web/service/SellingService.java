package com.bookstore.web.service;

import com.bookstore.web.model.Selling;

import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by Iulia on 19.05.2016.
 */

public interface SellingService {

    public List<Selling> listOfSellings();
    public Selling getSelling(int id);
    public void updateSelling(int id, int bookId, int employeeId, String customerName, int quantity, double price) throws JAXBException;
    public void deleteSelling(int id);
}
