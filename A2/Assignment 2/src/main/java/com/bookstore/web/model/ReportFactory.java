package com.bookstore.web.model;

import com.itextpdf.text.DocumentException;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * Created by Iulia on 10.05.2016.
 */
public class ReportFactory {

    public Report getReport(String reportType) {
        if (reportType == null) {
            return null;
        }
        if (reportType.equalsIgnoreCase("PDF")) {
            return new pdfReport();
        } else if (reportType.equalsIgnoreCase("CSV")) {
            return new csvReport();
        }
        return null;
    }

    public static void main(String args[]) throws DocumentException, JAXBException, IOException {
        ReportFactory reportFactory=new ReportFactory();
        Report report=reportFactory.getReport("PDF");
        report.generate();
    }
}
