package com.bookstore.web.dao;

import com.bookstore.web.model.User;

import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by Iulia on 08.05.2016.
 */
public interface UsersDAOInterface {

    public void createUser(int id, String username, String password, String name, int age, String type) throws JAXBException;
    public void deleteUser(int id) throws JAXBException;
    public User selectUser(int id) throws JAXBException;
    public void updateUser(int id, String username, String password, String name, int age, String type) throws JAXBException;
    public List<User> selectAllUsers() throws JAXBException;
    public boolean matchingCredentials(String username, String password) throws JAXBException;
    public User selectUserByUsername(String username) throws JAXBException;
}
