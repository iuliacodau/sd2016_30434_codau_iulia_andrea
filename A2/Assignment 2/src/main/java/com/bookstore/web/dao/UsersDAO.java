package com.bookstore.web.dao;

import com.bookstore.web.model.User;
import com.bookstore.web.model.Users;
import org.springframework.stereotype.Repository;

import javax.xml.bind.*;
import java.io.File;
import java.util.List;

/**
 * Created by Iulia on 08.05.2016.
 */

@Repository("userDAO")
public class UsersDAO implements UsersDAOInterface {


    JAXBContext jaxbContext=JAXBContext.newInstance(Users.class);
    Marshaller jaxbMarshaller=jaxbContext.createMarshaller();
    Unmarshaller jaxbUnmarshaller=jaxbContext.createUnmarshaller();
    private File usersXml=new File("E:\\sd2016_A2\\src\\main\\resources\\Users.xml");
    Users users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
   // users.setUsers(new ArrayList<User>());

    public UsersDAO() throws JAXBException {
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public void createUser(int id, String username, String password, String name, int age, String type) throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        //System.out.println(users.getUsers().size());
        User user=new User();
        user.setId(id);
        user.setAge(age);
        user.setName(name);
        user.setPassword(password);
        user.setType(type);
        user.setUsername(username);
        users.getUsers().add(user);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(users, usersXml);
    }

    @Override
    public void deleteUser(int id) throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        User deletedUser=new User();
        for (User user1 : users.getUsers()) {
            if (user1.getId() == id)
                deletedUser=user1;
        }
        users.getUsers().remove(deletedUser);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(users, usersXml);
    }

    @Override
    public User selectUser(int id) throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        User selectedUser=new User();
        for (User user1 : users.getUsers()) {
            if (user1.getId() == id) {
                selectedUser.setId(user1.getId());
                selectedUser.setUsername(user1.getUsername());
                selectedUser.setPassword(user1.getPassword());
                selectedUser.setAge(user1.getAge());
                selectedUser.setType(user1.getType());
                selectedUser.setName(user1.getName());
            }
        }
        return selectedUser; //not null
    }

    @Override
    public void updateUser(int id, String username, String password, String name, int age, String type) throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        User updatedUser=new User();
        User duplicate=new User();
        int index=0;
        for (User user1 : users.getUsers()) {
            if (user1.getId() == id) {
                index=users.getUsers().indexOf(user1);
                duplicate=user1;
                updatedUser.setUsername(username);
                updatedUser.setPassword(password);
                updatedUser.setAge(age);
                updatedUser.setType(type);
                updatedUser.setName(name);
                updatedUser.setId(id);
            }
        }
        users.getUsers().remove(duplicate);
        users.getUsers().add(index,updatedUser);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(users, usersXml);
    }

    @Override
    public List<User> selectAllUsers() throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        return users.getUsers();
    }

    @Override
    public boolean matchingCredentials(String username, String password) throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        for (User user1:users.getUsers()) {
            if ((user1.getUsername()!=null)
            && (user1.getPassword()!=null)
            &&(user1.getUsername().equals(username))
                    && (user1.getPassword().equals(password)))
                return true;
        }
        return false; //not null
    }

    @Override
    public User selectUserByUsername(String username) throws JAXBException {
        users = (Users) jaxbUnmarshaller.unmarshal(usersXml);
        User selectedUser=new User();
        for (User user1 : users.getUsers()) {
            if (user1.getUsername().equals(username) && (username!=null)) {
                selectedUser.setId(user1.getId());
                selectedUser.setUsername(user1.getUsername());
                selectedUser.setPassword(user1.getPassword());
                selectedUser.setAge(user1.getAge());
                selectedUser.setType(user1.getType());
                selectedUser.setName(user1.getName());
            }
        }
        return selectedUser;
    }
}
