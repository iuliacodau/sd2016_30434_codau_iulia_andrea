package com.bookstore.web.dao;

import com.bookstore.web.model.Book;
import com.bookstore.web.model.Books;
import org.springframework.stereotype.Repository;

import javax.xml.bind.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iulia on 08.05.2016.
 */

@Repository("bookDAO")
public class BooksDAO implements BooksDAOInterface{

    JAXBContext jaxbContext=JAXBContext.newInstance(Books.class);
    Marshaller jaxbMarshaller=jaxbContext.createMarshaller();
    Unmarshaller jaxbUnmarshaller=jaxbContext.createUnmarshaller();
    private File booksXml=new File("E:\\sd2016_A2\\src\\main\\resources\\Books.xml");
    Books books = (Books) jaxbUnmarshaller.unmarshal(booksXml);

    public BooksDAO() throws JAXBException {
    }

    public Books getBooks() {
        return books;
    }

    @Override
    public void createBook(int id, String title, String author, String genre, int quantity, double price) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        //System.out.println(users.getUsers().size());
        Book book=new Book();
        book.setId(id);
        book.setAuthor(author);
        book.setGenre(genre);
        book.setPrice(price);
        book.setQuantity(quantity);
        book.setTitle(title);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(books, booksXml);
    }

    @Override
    public Book selectBook(int id) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        Book selectedBook=new Book();
        for (Book book1 : books.getBooks()) {
            if (book1.getId() == id) {
                selectedBook.setTitle(book1.getTitle());
                selectedBook.setPrice(book1.getPrice());
                selectedBook.setQuantity(book1.getQuantity());
                selectedBook.setGenre(book1.getGenre());
                selectedBook.setAuthor(book1.getAuthor());
                selectedBook.setId(book1.getId());
            }
        }
        return selectedBook; //not null
    }

    @Override
    public List<Book> selectAllBooks() throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        return books.getBooks();
    }

    @Override
    public List<Book> selectBooksByGenre(String genre) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        List<Book> booksByGenre=new ArrayList<Book>();
        for (Book book1 : books.getBooks()) {
            if (book1.getGenre().equalsIgnoreCase(genre))
                booksByGenre.add(book1);
        }
        return booksByGenre;
    }

    @Override
    public List<Book> selectBooksByTitle(String title) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        List<Book> booksByTitle=new ArrayList<Book>();
        for (Book book1 : books.getBooks()) {
            if (book1.getTitle().equalsIgnoreCase(title))
                booksByTitle.add(book1);
        }
        return booksByTitle;
    }

    @Override
    public List<Book> selectBooksByAuthor(String author) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        List<Book> booksByAuthor=new ArrayList<Book>();
        for (Book book1 : books.getBooks()) {
            if (book1.getAuthor().equalsIgnoreCase(author))
                booksByAuthor.add(book1);
        }
        return booksByAuthor;
    }

    @Override
    public void deleteBook(int id) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        Book deletedBook=new Book();
        for (Book book1 : books.getBooks()) {
            if (book1.getId() == id)
                deletedBook=book1;
        }
        books.getBooks().remove(deletedBook);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(books, booksXml);


    }

    @Override
    public void updateBook(int id, String title, String author, String genre, int quantity, double price) throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        Book updatedBook=new Book();
        Book duplicate=new Book();
        int index=0;
        for (Book book1 : books.getBooks()) {
            if (book1.getId() == id) {
                index=books.getBooks().indexOf(book1);
                duplicate=book1;
                updatedBook.setId(id);
                updatedBook.setAuthor(author);
                updatedBook.setGenre(genre);
                updatedBook.setQuantity(quantity);
                updatedBook.setPrice(price);
                updatedBook.setTitle(title);
            }
        }
        books.getBooks().remove(duplicate);
        books.getBooks().add(index,updatedBook);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(books, booksXml);
    }

    @Override
    public List<Book> booksOnStock() throws JAXBException {
        books = (Books) jaxbUnmarshaller.unmarshal(booksXml);
        List<Book> booksOnStock=new ArrayList<Book>();
        for (int i=0; i<books.getBooks().size(); i++) {
            if (books.getBooks().get(i).getQuantity()>0)
                booksOnStock.add(books.getBooks().get(i));
        }
        return booksOnStock;

    }
}
