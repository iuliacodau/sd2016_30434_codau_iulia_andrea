package com.bookstore.web.model;

import com.bookstore.web.dao.BooksDAO;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.List;

/**
 * Created by Iulia on 10.05.2016.
 */
public class csvReport implements Report {

    private static final String COMMA_DELIMITER=",";
    private static final String NEW_LINE_SEPARATOR="\n";
    private static final String FILE_HEADER="id, title, author, genre, price";
    @Override
    public void generate() throws FileNotFoundException, JAXBException {
       // OutputStream file=new FileOutputStream(new File("E:\\sd2016_A2\\src\\main\\resources\\SalesReport.csv"));
        Document document=new Document();
        BooksDAO booksDAO=new BooksDAO();
        List<Book> list=booksDAO.selectAllBooks();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("E:\\sd2016_A2\\src\\main\\resources\\SalesReport.csv");
            fileWriter.append(FILE_HEADER.toString());
            fileWriter.append(NEW_LINE_SEPARATOR);
            for (int i=0; i<list.size(); i++){
                if (list.get(i).getQuantity()==0) {
                    fileWriter.append(String.valueOf(list.get(i).getId()));
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(list.get(i).getTitle());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(list.get(i).getAuthor());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(list.get(i).getGenre());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(String.valueOf(list.get(i).getPrice()));
                    fileWriter.append(NEW_LINE_SEPARATOR);
                }
            }
            System.out.println("CSV file was created successfully");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]) throws DocumentException, JAXBException, IOException {
        csvReport csvr=new csvReport();
        csvr.generate();
    }

}
