package com.bookstore.web.dao;

import com.bookstore.web.model.Book;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import java.util.List;

/**
 * Created by Iulia on 08.05.2016.
 */
public interface BooksDAOInterface {

    public void createBook(int id, String title, String author, String genre, int quantity, double price) throws JAXBException;
    public Book selectBook(int id) throws JAXBException;
    public List<Book> selectAllBooks() throws JAXBException;
    public List<Book> selectBooksByGenre(String genre) throws JAXBException;
    public List<Book> selectBooksByTitle(String title) throws JAXBException;
    public List<Book> selectBooksByAuthor(String author) throws JAXBException;
    public void deleteBook(int id) throws JAXBException;
    public void updateBook(int id, String title, String author, String genre, int quantity, double price) throws JAXBException;
    public List<Book> booksOnStock() throws JAXBException;
}
