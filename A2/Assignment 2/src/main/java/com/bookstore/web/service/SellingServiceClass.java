package com.bookstore.web.service;

import com.bookstore.web.dao.BooksDAO;
import com.bookstore.web.dao.SellingsDAO;
import com.bookstore.web.model.Book;
import com.bookstore.web.model.Selling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by Iulia on 19.05.2016.
 */

@Service("sellingService")
public class SellingServiceClass implements SellingService {

    @Autowired
    private SellingsDAO sellingsDAO=new SellingsDAO();
    @Autowired
    private BooksDAO booksDAO=new BooksDAO();

    public SellingServiceClass() throws JAXBException {
    }

    @Override
    public List<Selling> listOfSellings() {
        return null;
    }

    @Override
    public Selling getSelling(int id) {
        return null;
    }

    @Override
    public void updateSelling(int id, int bookId, int employeeId, String customerName, int quantity, double price) throws JAXBException {
        sellingsDAO.createSelling(id, bookId, employeeId, quantity, price, customerName);
        Book b=booksDAO.selectBook(bookId);
        booksDAO.updateBook(b.getId(), b.getTitle(), b.getAuthor(),b.getGenre(), b.getQuantity()-quantity, b.getPrice());
    }

    @Override
    public void deleteSelling(int id) {

    }
}
