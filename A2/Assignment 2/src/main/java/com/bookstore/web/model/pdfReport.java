package com.bookstore.web.model;

import com.bookstore.web.dao.BooksDAO;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.List;

/**
 * Created by Iulia on 10.05.2016.
 */
public class pdfReport implements Report {
    @Override
    public void generate() throws IOException, DocumentException, JAXBException {
        OutputStream file=new FileOutputStream(new File("E:\\sd2016_A2\\src\\main\\resources\\SalesReport.pdf"));
        Document document=new Document();
        PdfWriter.getInstance(document, file);
        BooksDAO booksDAO=new BooksDAO();
        List<Book> list=booksDAO.selectAllBooks();
        document.open();
        document.add(new Paragraph("Sold out books are :"));
        for (int i=0; i<list.size(); i++){
            if (list.get(i).getQuantity()==0)
                document.add(new Paragraph(list.get(i).toString()));
        }
        document.close();
        file.close();
        System.out.println("Report created");
    }

    public static void main(String args[]) throws DocumentException, JAXBException, IOException {
        pdfReport pdfr=new pdfReport();
        pdfr.generate();
    }
}
