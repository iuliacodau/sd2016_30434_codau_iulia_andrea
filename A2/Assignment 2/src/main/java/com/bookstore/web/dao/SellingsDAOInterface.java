package com.bookstore.web.dao;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

/**
 * Created by Iulia on 08.05.2016.
 */
public interface SellingsDAOInterface {

    public void createSelling(int id, int bookId, int employeeId, int quantity, double price, String customerName) throws JAXBException;

}
