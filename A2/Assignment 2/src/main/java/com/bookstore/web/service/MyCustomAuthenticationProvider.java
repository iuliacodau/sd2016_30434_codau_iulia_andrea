package com.bookstore.web.service;

import com.bookstore.web.dao.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Iulia on 11.05.2016.
 */


@Service("userDetailsService")
public class MyCustomAuthenticationProvider implements UserDetailsService {

    @Autowired
    private UsersDAO usersDAO=new UsersDAO();

    public MyCustomAuthenticationProvider() throws JAXBException {
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            //com.bookstore.web.model.User user=usersDAO.selectUserByUsername(username);
            com.bookstore.web.model.User user=usersDAO.selectUserByUsername(username);
            System.out.println("CACACACACAC usera" + username + user.toString());
            System.out.println("ESDRCFGVHBJ " + username+"  esfrdtf "+ user.getType());
            List<GrantedAuthority> authorities=createUserAuthority(user.getType());
            return createUserForAuthentication(user, authorities);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    private User createUserForAuthentication(com.bookstore.web.model.User user, List<GrantedAuthority> authorities) {
        System.out.println("FCCC "+user.getPassword()+user.getUsername());
        return new User(user.getUsername(), user.getPassword(), true, true, true, true, authorities );
    }

    private List<GrantedAuthority> createUserAuthority(String userRole) {
        Set<GrantedAuthority> setAuthorities = new HashSet<GrantedAuthority>();
        setAuthorities.add(new SimpleGrantedAuthority(userRole));
        List<GrantedAuthority> Result=new ArrayList<GrantedAuthority>(setAuthorities);
        return Result;

    }
}










