package com.bookstore.web.service;

import com.bookstore.web.dao.BooksDAO;
import com.bookstore.web.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by Iulia on 10.05.2016.
 */

@Service("bookService")
public class BookServiceClass implements BookService {

    @Autowired
    private BooksDAO booksDAO=new BooksDAO();

    public BookServiceClass() throws JAXBException {

    }

    @Override
    public List<Book> listOfBooks() throws JAXBException {
        return booksDAO.selectAllBooks();
    }

    @Override
    public Book getBook(int id) throws JAXBException {
        return booksDAO.selectBook(id);
    }

    @Override
    public void updateBook(int id, String title, String author, String genre, int quantity, double price) throws JAXBException {
        if (id!=0)
            booksDAO.updateBook(id, title, author, genre, quantity, price);
        else booksDAO.createBook(id, title, author, genre, quantity, price);
    }

    @Override
    public void deleteBook(int id) throws JAXBException {
        booksDAO.deleteBook(id);
    }

    @Override
    public List<Book> listOfBooksbyGenre(String genre) throws JAXBException {
        return booksDAO.selectBooksByGenre(genre);
    }

    @Override
    public List<Book> listOfBooksbyTitle(String title) throws JAXBException {
        return booksDAO.selectBooksByTitle(title);
    }

    @Override
    public List<Book> listOfBooksbyAuthor(String author) throws JAXBException {
        return booksDAO.selectBooksByAuthor(author);
    }

}
