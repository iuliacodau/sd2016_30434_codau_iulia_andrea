package com.bookstore.web.controller;

import com.bookstore.web.model.Report;
import com.bookstore.web.model.ReportFactory;
import com.bookstore.web.model.Selling;
import com.bookstore.web.model.User;
import com.bookstore.web.service.UserService;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sun.misc.IOUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.AbstractDocument;
import javax.xml.bind.JAXBException;
import java.io.*;

/**
 * Created by Iulia on 09.05.2016.
 */

@Controller
//@RequestMapping("/user")
public class UserController {

   // private static final int DEFAULT_BUFFER_SIZE = 100;

    @Autowired
    private UserService userService;

    //Select all users
    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public String selectUsers(ModelMap map) throws JAXBException {
        map.put("userList", userService.listOfUsers());
        return "/select";
    }

    //update user form
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String update(@PathVariable(value="id") int id,  ModelMap map, HttpServletRequest request) throws JAXBException {
        map.put("user", userService.getUser(id));
        map.put("cp", request.getContextPath());
        return "/update";
    }

    //update user
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@ModelAttribute("user")User user, ModelMap map) throws JAXBException {
        userService.updateUser(user.getId(), user.getUsername(), user.getPassword(), user.getName(), user.getAge(), user.getType());
        //redirect to prevent refresh button or back button posting the form again
        System.out.println("I am about to update a  user");
        return "redirect:/select";
    }

    //create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(ModelMap map, HttpServletRequest request) {
        map.put("cp", request.getContextPath());
        map.put("user", new User());
        System.out.println("I am about to create a new user ");
        return "update";
    }


    //delete form
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id")int id, ModelMap map, HttpServletRequest request) throws JAXBException {
        map.put("cp", request.getContextPath());
        map.put("user", userService.getUser(id));
        return "/delete";
    }

    //actually delete user
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@ModelAttribute User user) throws JAXBException {
        userService.deleteUser(user.getId());
        return "redirect:/select";
    }

   /* @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public String deleteUser(@RequestParam("id") int id) throws JAXBException {
        userService.deleteUser(id);
        return "redirect:/select";
    }*/

    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.POST)
    public String deleteUser(@ModelAttribute User user) throws JAXBException {
        System.out.println("Deleting user "+user.getId());
        userService.deleteUser(user.getId());
        return "redirect:/select";
    }

    /*
    @RequestMapping(value = "/delete_element", method = RequestMethod.GET)
    public String getInfo(@RequestParam("user.id") int id) throws JAXBException {
        userService.deleteUser(id);
        return "/select";
    }

    @RequestMapping(value = "/delete_element", method = RequestMethod.GET)
    public String getInfo(@ModelAttribute User user) throws JAXBException {
        userService.deleteUser(user.getId());
        return "redirect:/select";
    }*/

    @RequestMapping(value = "/delete_element/{id}", method = RequestMethod.DELETE)
    public String deleteElementById(@PathVariable("id")int id, ModelMap map, HttpServletRequest request) throws JAXBException {
        userService.deleteUser(id);
        return "redirect:/select";
    }

    @RequestMapping("/deleteNew/{id}")
    public String deleteNew(@PathVariable int id, RedirectAttributes redirectAttr) throws JAXBException {
        userService.deleteUser(id);
        System.out.println("Here");
        redirectAttr.addFlashAttribute("message", "User was deleted");
        return "redirect:/select";
    }

}
