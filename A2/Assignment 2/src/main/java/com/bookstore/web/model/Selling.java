package com.bookstore.web.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Iulia on 08.05.2016.
 */

@XmlRootElement(name = "selling")
@XmlAccessorType(XmlAccessType.FIELD)
public class Selling {

    private int id;
    private int bookId;
    private int employeeId;
    private String customerName;
    private int quantity;
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Selling{" +
                "id=" + id +
                ", bookId=" + bookId +
                ", employeeId=" + employeeId +
                ", customerName='" + customerName + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
