package com.bookstore.web.service;

import com.bookstore.web.dao.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iulia on 09.05.2016.
 */

//@Component
@Service
public class customAuthenticationProvider implements AuthenticationProvider{

    @Autowired
    private UsersDAO usersDAO=new UsersDAO();

    public customAuthenticationProvider() throws JAXBException {
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username=authentication.getName();
        String password=authentication.getCredentials().toString();
        try {
            if (authorizedUser(username, password)) {
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(() -> {
                    return "AUTH_USER";
                });
                Authentication auth = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);
                System.out.println(auth.getAuthorities());
                return auth;
            } else {
                throw new AuthenticationCredentialsNotFoundException("Invalid credentials");
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean authorizedUser(String username, String password) throws JAXBException {
        System.out.println("Username is "+username+" and the password "+password);
        if (usersDAO.matchingCredentials(username, password))
      //  if ("Iulia".equals(username) && "Iulia".equals(password))
            return true;
        return false;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}




























