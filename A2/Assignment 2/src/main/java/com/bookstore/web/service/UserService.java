package com.bookstore.web.service;

import com.bookstore.web.model.Report;
import com.bookstore.web.model.ReportFactory;
import com.bookstore.web.model.User;

import javax.xml.bind.JAXBException;
import java.util.List;

/**
 * Created by Iulia on 09.05.2016.
 */
public interface UserService {



    public List<User> listOfUsers() throws JAXBException;
    public User getUser(int id) throws JAXBException;
    public void updateUser(int id, String username, String password, String name, int age, String type) throws JAXBException;
    public void deleteUser(int id) throws JAXBException;
}
