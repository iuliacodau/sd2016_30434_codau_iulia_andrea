'use strict';

App.controller('CabController', ['$scope', 'CabService', function($scope, CabService) {
    var self = this;
    self.cab={ id:null, driver:'', rating:'',location:''};
    self.cabs=[];

    self.fetchAllCabs = function(){
        CabService.fetchAllCabs()
            .then(
                function(d) {
                    self.cabs = d;
                },
                function(errResponse){
                    console.error('Error while fetching cabs');
                }
            );
    };

    self.createCab = function(cab){
        CabService.createCab(cab)
            .then(
                self.fetchAllCabs,
                function(errResponse){
                    console.error('Error while creating cab.');
                }
            );
    };

    self.updateCab = function(cab, id){
        CabService.updateCab(cab, id)
            .then(
                self.fetchAllCabs,
                function(errResponse){
                    console.error('Error while updating cab.');
                }
            );
    };

    self.deleteCab = function(id){
        CabService.deleteCab(id)
            .then(
                self.fetchAllCabs,
                function(errResponse){
                    console.error('Error while deleting cab.');
                }
            );
    };

    self.fetchAllCabs();

    self.submit = function() {
        if(self.cab.id===null){
            console.log('Saving New patient', self.cab);
            self.createCab(self.cab);
        }else{
            self.updateCab(self.cab, self.cab.id);
            console.log('Patient updated with id ', self.cab.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.cabs.length; i++){
            if(self.cabs[i].id === id) {
                self.cab = angular.copy(self.cabs[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.cab.id === id) {//clean form if the user to be deleted is shown there.
            self.reset();
        }
        self.deleteCab(id);
    };


    self.reset = function(){
        self.cab={ id:null, driver:'', rating:'',location:''};
        $scope.myForm.$setPristine(); //reset Form
    };

}]);