package com.cabcaller.web.controller;

import com.cabcaller.web.dao.OrderDAO;
import com.cabcaller.web.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class OrderController {

    @Autowired
    OrderDAO orderDAO;

    //Creating a new user
    @RequestMapping(value = "/order", method = RequestMethod.POST, consumes = "application/JSON", produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Order> createNewOrder(@RequestBody Order order) {
        try {
            orderDAO.create(order);
        } catch (Exception e) {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders hdr=new HttpHeaders();
        hdr.add("Location", "/rest/orders/" + order.getId());
        return new ResponseEntity<Order>(order, hdr, HttpStatus.CREATED);
    }

    //Select user according to its id (in order to update or delete later
    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET, produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Order> selectOrder(@PathVariable String id) {
        try {
            Order order = orderDAO.findById(id);
            return new ResponseEntity<Order>(order, HttpStatus.FOUND);
        } catch(Exception e) {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    //Get all the users from the database
    @RequestMapping (value = "/orders", method = RequestMethod.GET, produces="application/JSON")
    @ResponseBody
    public ResponseEntity<List<Order>> showOrders(Model model) {
        List<Order> orderList;
        try {
            model.addAttribute("order", new Order());
            model.addAttribute("orderList", orderDAO.find());
            orderList=orderDAO.find();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } return new ResponseEntity<>(orderList, HttpStatus.OK);
    }

    //Updating a user
    @RequestMapping (value = "/orders/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Order> updateOrder(@PathVariable("id") String id, @RequestBody Order order) {
        try {
            if (orderDAO.findById(id).getId().equals(id)) {
                order.setId(id);
                orderDAO.update(order);
            }
        } catch (Exception e) {
            return new ResponseEntity<Order>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Order>(order, HttpStatus.OK);
    }

    //Deleting a user
    @RequestMapping (value = "/orders/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") String id) {
        try {
            orderDAO.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<Order>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Order>(HttpStatus.OK);
    }
}
