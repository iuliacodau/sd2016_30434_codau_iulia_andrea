package com.cabcaller.web.dao;

import com.cabcaller.web.model.Cab;
import com.cabcaller.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Repository(value = "CAB")
public class CabDAOImplementation implements CabDAO{

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void create(Cab cab) {
        mongoTemplate.insert(cab);
    }

    @Override
    public void update(Cab cab) {
        mongoTemplate.save(cab);
    }

    @Override
    public void delete(String id) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id)), Cab.class);
    }

    @Override
    public List<Cab> find() {
        return mongoTemplate.findAll(Cab.class);
    }

    @Override
    public Cab findById(String id) {
        Cab cab=new Cab();
        List<Cab> cabList=mongoTemplate.find(Query.query(Criteria.where("id").is(id)), Cab.class);
        if (!cabList.isEmpty()) {
            cab=cabList.get(0);
        }
        return cab;
    }

    @Override
    public Cab findByRating(String rating) {
        Cab cab=new Cab();
        List<Cab> cabList=mongoTemplate.find(Query.query(Criteria.where("rating").is(rating)), Cab.class);
        if(!cabList.isEmpty()){
            cab=cabList.get(0);
        }
        return cab;
    }
}
