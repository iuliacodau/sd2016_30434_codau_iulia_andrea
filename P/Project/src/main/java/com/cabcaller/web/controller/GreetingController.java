package com.cabcaller.web.controller;

import com.cabcaller.web.model.Greeting;
import com.cabcaller.web.model.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by Iulia on 26.05.2016.
 */

@Controller
public class GreetingController {

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(Message message) throws Exception {
        return new Greeting ("New order from Customer : " + message.getName() +"!!");
    }
}
