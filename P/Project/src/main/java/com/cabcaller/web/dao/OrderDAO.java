package com.cabcaller.web.dao;

import com.cabcaller.web.model.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Component
public interface OrderDAO {

    public void create(Order order);
    public void update(Order order);
    public void delete(String id);
    public List<Order> find();
    public Order findById(String id);
}
