package com.cabcaller.web.dao;

import com.cabcaller.web.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Component
public interface UserDAO {

    public void create(User user);
    public void update(User user);
    public void delete(String id);
    public List<User> find();
    public User findById(String id);
}
