<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 23.05.2016
  Time: 18:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cab Caller</title>


    <spring:url value="/resources/core/css/stylish-portfolio.css" var="coreCss" />
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>

<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
        <li class="sidebar-brand">
            <a href="#top"  onclick = $("#menu-close").click(); >CabCaller</a>
        </li>
        <li>
            <a href="#about" onclick = $("#menu-close").click(); >About</a>
        </li>
        <li>
            <a href="#services" onclick = $("#menu-close").click(); >Users</a>
        </li>
        <li>
            <a href="/logout" >Logout</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<header id="top" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Cab Caller </h2>
            </div>
        </div>
    </div>
</header>

<!-- About -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Admin </h2>
                <p class="lead">Username : ${username}.</p>
                <p class="lead">Admin can perform CRUD <a href="/usersInfo">on users</a> and <a href="/cabsInfo">cabs</a> </p>
            </div>
        </div>
    </div>
</section>

<!-- Call to Action -->
<aside class="call-to-action bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3>Cab orders</h3>
                <a href="/index" class="btn btn-lg btn-light">Check all</a>
                <a href="/cabsInfo" class="btn btn-lg btn-dark">Crud on cabs</a>
                <a href="/usersInfo" class="btn btn-lg btn-dark">Crud on users</a>
                <a href="/ordersInfo" class="btn btn-lg btn-dark">Assign Cabst</a>
            </div>
        </div>
    </div>
</aside>

<!-- Map -->
<section id="contact" class="map">
    <div class="col-lg-12 text-center">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d174852.23585320893!2d23.476427960194275!3d46.783300191375154!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47490c1f916c0b8b%3A0xbbc601c331f148b!2sCluj-Napoca!5e0!3m2!1sro!2sro!4v1464235752429" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>


<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <hr class="small">
                <p class="text-muted">Copyright &copy; CabCaller</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="/resources/core/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/core/js/bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
</script>





</body>
</html>
