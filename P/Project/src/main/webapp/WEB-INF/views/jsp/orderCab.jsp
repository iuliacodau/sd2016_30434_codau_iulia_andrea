<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="myApp">
<head>
    <title>All active orders</title>
    <style>
        .username.ng-valid {
            background-color: lightgreen;
        }
        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .username.ng-dirty.ng-invalid-minlength {
            background-color: yellow;
        }

        .email.ng-valid {
            background-color: lightgreen;
        }
        .email.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .email.ng-dirty.ng-invalid-email {
            background-color: yellow;
        }

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body  class="ng-cloak">
<div class="generic-container" ng-controller="OrderController as ctrl">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">Order a Cab</span></div>

                <div class="formcontainer">
                    <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                        <input type="hidden" ng-model="ctrl.order.id" />


                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="col-md-2 control-lable" for="cabId">Cab ID</label>

                                <select name="singleSelect" ng-model="selectedCab">
                                    <option ng-repeat="cab in ctrl.cabs" value="{{cab.id}}">{{cab.id}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="col-md-2 control-lable" for="startLocation">Start Location</label>
                                <div class="col-md-7">
                                    <input type="text" ng-model="ctrl.order.startLocation" id="startLocation" class="startLocation form-control input-sm" placeholder="Enter your name" required ng-minlength="3"/>
                                    <div class="has-error" ng-show="myForm.$dirty">
                                        <span ng-show="myForm.startLocation.$error.required">This is a required field</span>
                                        <span ng-show="myForm.startLocation.$error.minlength">Minimum length required is 3</span>
                                        <span ng-show="myForm.startLocation.$invalid">This field is invalid </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="col-md-2 control-lable" for="endLocation">End Location</label>
                                <div class="col-md-7">
                                    <input type="text" ng-model="ctrl.order.endLocation" id="endLocation" class="endLocation form-control input-sm" placeholder="Enter your name" required ng-minlength="3"/>
                                    <div class="has-error" ng-show="myForm.$dirty">
                                        <span ng-show="myForm.endLocation.$error.required">This is a required field</span>
                                        <span ng-show="myForm.endLocation.$error.minlength">Minimum length required is 3</span>
                                        <span ng-show="myForm.endLocation.$invalid">This field is invalid </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-actions floatRight">
                                <input type="submit" ng-click="ctrl.createOrder(selectedCab,ctrl.order)"  value="{{!ctrl.order.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                                <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                            </div>
                        </div>
                    </form>
                </div>


   <%-- </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Orders </span></div>
        <div class="tablecontainer">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Cab ID</th>
                    <th>Start Location</th>
                    <th>End Location</th>
                    <th width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="u in ctrl.orders">
                    <td><span ng-bind="u.cabId"></span></td>
                    <td><span ng-bind="u.startLocation"></span></td>
                    <td><span ng-bind="u.endLocation"></span></td>
                    <td>
                        &lt;%&ndash; <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Edit</button>&ndash;%&gt;
                        <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Send Cab</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>--%>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/resources/core/js/app.js' />"></script>
<script src="<c:url value='/resources/core/js/service/order_service.js' />"></script>
<script src="<c:url value='/resources/core/js/service/cab_service.js' />"></script>
<script src="<c:url value='/resources/core/js/controller/order_controller.js' />"></script>
</body>
</html>