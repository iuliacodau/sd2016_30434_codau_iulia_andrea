package dataAccess;

import java.text.ParseException;
import java.util.ArrayList;

public interface ReportDAO {

	public ArrayList<ReportDB> findReportRange(int uid, String inputStr,  String inputStr1) throws ParseException;
	public void insertReport(ReportDB report) throws ParseException;
	public ArrayList<ReportDB> findAllReports(int uid);
	
}
