package dataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserGateway implements UserDAO {


	Statement st=null;
	ResultSet rs=null;
	
	@Override
	public void insertUser(String type, String username, String password) {
		// TODO Auto-generated method stub
		try{
		Connection connection=ConnectionSingleton.getInstance().getConnection();
		String qs="INSERT INTO User (Type, Username, password) VALUES ('"+type+"','"+username+"', '"+password+"')";
		st=connection.createStatement();
		st.executeUpdate(qs);
		System.out.println("Data inserted: "+type+" "+username+" "+password );
	}catch (SQLException e){
		e.printStackTrace();
	}
	}
	
	@Override
	public void deleteUser(int uid) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="DELETE FROM User WHERE User_ID='"+uid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data deleted: "+uid);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public String findUser(int uid) {
		// TODO Auto-generated method stub
		String nameFound=new String();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM User WHERE User_ID='"+uid+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				//rs.getInt("Client_ID");
				//rs.getInt("User_ID");
				//rs.getBoolean("xxxx");
				//rs.getInt("xxxxxx");
				nameFound =rs.getString("Name");
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return nameFound;
	}
	
/*	public int findId(String username) {
		// TODO Auto-generated method stub
		int found=-1;
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT User_ID FROM User WHERE Username='"+username+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				found =rs.getInt("User_ID");
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return found;
	}*/
	
	@Override
	public void updateName(String name, int uid) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void updatePassword(String pass, int uid) {
		// TODO Auto-generated method stub
	}

	@Override
	public String findUserPassword(String username) {
		// TODO Auto-generated method stub
		
		String passwordFound=new String();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM User WHERE User_ID='"+username+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				passwordFound =rs.getString("Password");
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return passwordFound;
	}
	
	public ResultSet slectUser(String username) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM User WHERE Username='"+username+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				rs.getInt("Client_ID");
				rs.getInt("User_ID");
				rs.getString("Name");
				rs.getInt("CNP");
				rs.getString("Address");
				rs.getString("CardId");
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return rs;
	}
	
	public UserDB findUserByUsername(String username)
	{
			UserDB foundUser=new UserDB();
			try{
				Connection connection=ConnectionSingleton.getInstance().getConnection();
				String qs="SELECT * FROM User WHERE Username='"+username+"'";
				st=connection.createStatement();
				rs=st.executeQuery(qs);
				while(rs.next()){
					foundUser.setUid(rs.getInt("User_ID"));
					foundUser.setType(rs.getString("Type"));
					foundUser.setUsername(rs.getString("Username"));
					foundUser.setPassword(rs.getString("Password"));
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return foundUser;
	}
	
	public ArrayList<UserDB> findAllUsers() {
		// TODO Auto-generated method stub
		ArrayList<UserDB> listOfUsers=new ArrayList<UserDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM User";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				UserDB userFromList=new UserDB();
				userFromList.setUid(rs.getInt("User_ID"));
				userFromList.setType(rs.getString("Type"));
				userFromList.setUsername(rs.getString("Username"));
				userFromList.setPassword(rs.getString("Password"));
				System.out.println(userFromList.toString());
				listOfUsers.add(userFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfUsers;
	}
	
	public static void main(String[] args) throws SQLException {
		ArrayList<UserDB> users=new ArrayList<UserDB>();
		UserGateway ug=new UserGateway();
		users=ug.findAllUsers();
		for (int i=0; i<users.size();i++){
			System.out.println(users.get(i).toString());
		}
		//System.out.println("This is a user "+ users.get(1).toString());
	}

	public void updateUser(int uid, UserDB updatedData) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE User SET Type='"+updatedData.getType()+"', Username='"+updatedData.getUsername()+"', Password='"+updatedData.getPassword()+"' WHERE User_ID='"+uid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data updated: "+updatedData.toString());
		}catch (SQLException e){
			e.printStackTrace();
		}
	}

}
