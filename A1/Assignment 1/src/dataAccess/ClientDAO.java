package dataAccess;

public interface ClientDAO {

	public void insertClient(ClientDB client);
	public void deleteClient(int clientId);
	public ClientDB findClientByName(String name);
}
