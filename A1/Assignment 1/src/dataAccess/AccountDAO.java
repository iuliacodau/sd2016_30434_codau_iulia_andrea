package dataAccess;

public interface AccountDAO {

	public void insertAccount(int amount, String creation, String type, int aid, int cid);
	public void deleteAccount(int aid);
	public void updateAmount(int amount, int aid);
	
}
