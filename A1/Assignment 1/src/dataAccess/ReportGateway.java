package dataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ReportGateway implements ReportDAO{
	
	Statement st=null;
	ResultSet rs=null;	
	
	public ArrayList<ReportDB> findReportRange(int uid, String inputStr,  String inputStr1) throws ParseException {
		// TODO Auto-generated method stub
		
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    java.util.Date inputDate = dateFormat.parse(inputStr);
		java.sql.Date sqlDate = new java.sql.Date(inputDate.getTime()); 
		System.out.println("Hello " + sqlDate);
		
		
	    //String inputStr1 = "2012-11-30";
	    DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
	    java.util.Date inputDate1 = dateFormat1.parse(inputStr1);
		java.sql.Date sqlDate1 = new java.sql.Date(inputDate1.getTime()); 
		System.out.println("Hello " + sqlDate1);
		
		ArrayList<ReportDB> listOfReports=new ArrayList<ReportDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM report where User_ID='"+uid+"' and DateOfAction between '"+sqlDate+"' and '"+sqlDate1+"' order by DateOfAction ASC";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				ReportDB reportFromList=new ReportDB();
				reportFromList.setRid(rs.getInt("Report_ID"));
				reportFromList.setUid(rs.getInt("User_ID"));
				reportFromList.setType(rs.getString("Type"));
				reportFromList.setDoa(rs.getString("DateOfAction"));
				System.out.println(reportFromList.toString());
				listOfReports.add(reportFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfReports;
	}
	
	public void insertReport(ReportDB report) throws ParseException {
		// TODO Auto-generated method stub
		try{	
		Connection connection=ConnectionSingleton.getInstance().getConnection();
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    java.util.Date inputDate = dateFormat.parse(report.getDoa());
		java.sql.Date sqlDate = new java.sql.Date(inputDate.getTime()); 
		System.out.println("Hello " + sqlDate);
		String qs="INSERT INTO report(Type, DateOfAction, User_ID) VALUES ('"+report.getType()+"','"+sqlDate
		+"','"+report.getUid()+"')";
		st=connection.createStatement();
		st.executeUpdate(qs);
		//System.out.println("Data inserted: " + amount + " "+creation+" "+type );
	}catch (SQLException e){
		e.printStackTrace();
	}
		
	}
	
	public ArrayList<ReportDB> findAllReports(int uid) {
		// TODO Auto-generated method stub
		ArrayList<ReportDB> listOfReports=new ArrayList<ReportDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM report where User_ID='"+uid+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				ReportDB reportFromList=new ReportDB();
				reportFromList.setRid(rs.getInt("Report_ID"));
				reportFromList.setType(rs.getString("Type"));
				reportFromList.setDoa(rs.getString("DateOfAction"));
				reportFromList.setUid(rs.getInt("User_ID"));
				System.out.println(reportFromList.toString());
				listOfReports.add(reportFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfReports;
	}
	
	/*
	public static void main(String[] args) throws ParseException{
	ReportGateway sm=new ReportGateway();

	
    String inputStr = "2012-04-01";
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    java.util.Date inputDate = dateFormat.parse(inputStr);
    
	//java.util.Date inputdate = new java.util.Date();
	java.sql.Date sqlDate = new java.sql.Date(inputDate.getTime()); 
	System.out.println("Hello " + sqlDate);
	
	
    String inputStr1 = "2012-11-30";
    DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    java.util.Date inputDate1 = dateFormat1.parse(inputStr1);
    
	//java.util.Date inputdate = new java.util.Date();
	java.sql.Date sqlDate1 = new java.sql.Date(inputDate1.getTime()); 
	System.out.println("Hello " + sqlDate1);
	

	ReportDB report=new ReportDB();
	report.setUid(1);
	report.setDoa("2012-10-10");
	report.setType("new client creation");
	sm.insertReport(report);
	sm.findReportRange(26, "2012-04-01", "2012-11-30");
}*/
}
