package dataAccess;

public interface UserDAO {
	
	public void insertUser(String type, String username, String password);
	public void deleteUser(int uid);
	public String findUser(int uid);
	public String findUserPassword(String username);
	public void updateName(String name, int uid);
	public void updatePassword(String pass, int uid);
	public UserDB findUserByUsername(String username);
}
