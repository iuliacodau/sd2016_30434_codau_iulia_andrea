package dataAccess;


public class ReportDB {

	private int rid;
	private int uid;
	private String type;
	private String doa;
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	@Override
	public String toString() {
		return "ReportDB [rid=" + rid + ", cid=" + uid + ", type=" + type + ", doa=" + doa + "]";
	}
	
	
	
	
}
