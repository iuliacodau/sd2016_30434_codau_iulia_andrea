package dataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UtilityGateway implements UtilityDAO {

	Statement st=null;
	ResultSet rs=null;
	
	@Override
	public void insertUtility(int uid, String type, int amount){
		try{
		Connection connection=ConnectionSingleton.getInstance().getConnection();
		String qs="INSERT INTO Utility VALUES ('"+uid+"','"+type+"','"+amount+"')";
		st=connection.createStatement();
		st.executeUpdate(qs);
		System.out.println("Data inserted: " + uid + " "+type+" "+amount );
	}catch (SQLException e){
		e.printStackTrace();
	}
}		

	@Override
	public ResultSet findUtility(int uid) {
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM Utility WHERE Utility_ID='"+uid+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				rs.getInt("Utility_ID");
				rs.getBoolean("Type");
				rs.getInt("Amount");
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return rs;
	}

	@Override
	public void deleteUtility(int uid) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="DELETE FROM Utility WHERE Utility_ID='"+uid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data deleted: "+uid);
		}catch (SQLException e){
			e.printStackTrace();
		}	
	}

	@Override
	public void updateType(String type, int uid) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Utility SET Type='"+type+"' WHERE Utility_ID='"+uid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + type);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void updateAmount(int amount, int uid) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Utility SET Amount='"+amount+"' WHERE Utility_ID='"+uid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + amount);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public ArrayList<UtilityDB> findAllUtilities(int cid) {
		// TODO Auto-generated method stub
		ArrayList<UtilityDB> listOfUtilities=new ArrayList<UtilityDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM utility where Client_ID='"+cid+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				UtilityDB utilityFromList=new UtilityDB();
				utilityFromList.setType(rs.getString("Type"));
				utilityFromList.setAmount(rs.getInt("Amount"));
				utilityFromList.setCid(rs.getInt("Client_ID"));
				utilityFromList.setUid(rs.getInt("Utility_ID"));
				System.out.println(utilityFromList.toString());
				listOfUtilities.add(utilityFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfUtilities;
	}
	
	public UtilityDB findUtilityById(int id)
	{
		UtilityDB foundUtility=new UtilityDB();
			try{
				Connection connection=ConnectionSingleton.getInstance().getConnection();
				String qs="SELECT * FROM Utility WHERE Utility_ID='"+id+"'";
				st=connection.createStatement();
				rs=st.executeQuery(qs);
				while(rs.next()){
					foundUtility.setUid(rs.getInt("Utility_ID"));
					foundUtility.setCid(rs.getInt("Client_ID"));
					foundUtility.setAmount(rs.getInt("Amount"));
					foundUtility.setType(rs.getString("Type"));
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return foundUtility;
	}
	
	/*public static void main(String[] args){
		Account acc=new Account(700, new Date(28-03-2016), true, 1, 1);
		acc.addSum(200,1);
	}*/
	
}
