package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSingleton {
	
	private static ConnectionSingleton connectionSingleton;

	private ConnectionSingleton(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}	
	}
	
	public Connection getConnection() {
		Connection conn=null;
		try {
			conn=(Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/bank?autoReconnect=true&useSSL=false", "root", "root");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (conn!=null){
			System.out.println("Connected");
		}
		return conn;
	}
	
	public static ConnectionSingleton getInstance(){
		if (connectionSingleton==null){
			connectionSingleton=new ConnectionSingleton();
		}
		return connectionSingleton;
	}

}
