package dataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClientGateway implements ClientDAO{


	Statement st=null;
	ResultSet rs=null;
	
	@Override
	public void insertClient(ClientDB client) {
		// TODO Auto-generated method stub
		try{	
		Connection connection=ConnectionSingleton.getInstance().getConnection();
		String qs="INSERT INTO Client(User_ID, Name, CNP, Address, CardId) VALUES ('"+client.getUid()+"','"+client.getName()
		+"','"+client.getCnp()+"', '"+client.getAddress()+"', '"+client.getCardid()+"')";
		st=connection.createStatement();
		st.executeUpdate(qs);
		//System.out.println("Data inserted: " + amount + " "+creation+" "+type );
	}catch (SQLException e){
		e.printStackTrace();
	}
		
	}

	@Override
	public void deleteClient(int clientId) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="DELETE FROM Client WHERE Client_ID='"+clientId+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data deleted: "+clientId);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public ClientDB findClientByName(String name)
	{
			ClientDB foundClient=new ClientDB();
			try{
				Connection connection=ConnectionSingleton.getInstance().getConnection();
				String qs="SELECT * FROM Client WHERE Name='"+name+"'";
				st=connection.createStatement();
				rs=st.executeQuery(qs);
				while(rs.next()){
					foundClient.setCid(rs.getInt("Client_ID"));
					foundClient.setUid(rs.getInt("User_ID"));
					foundClient.setName(rs.getString("Name"));
					foundClient.setCnp(rs.getString("CNP"));
					foundClient.setAddress(rs.getString("Address"));
					foundClient.setCardid(rs.getString("CardId"));
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return foundClient;
	}
	
	public ClientDB findClientByCnp(String cnp)
	{
			ClientDB foundClient=new ClientDB();
			try{
				Connection connection=ConnectionSingleton.getInstance().getConnection();
				String qs="SELECT * FROM Client WHERE CNP='"+cnp+"'";
				st=connection.createStatement();
				rs=st.executeQuery(qs);
				while(rs.next()){
					foundClient.setCid(rs.getInt("Client_ID"));
					foundClient.setUid(rs.getInt("User_ID"));
					foundClient.setName(rs.getString("Name"));
					foundClient.setCnp(rs.getString("CNP"));
					foundClient.setAddress(rs.getString("Address"));
					foundClient.setCardid(rs.getString("CardId"));
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return foundClient;
	}
	
	public ClientDB findClientById(int id)
	{
			ClientDB foundClient=new ClientDB();
			try{
				Connection connection=ConnectionSingleton.getInstance().getConnection();
				String qs="SELECT * FROM Client WHERE Client_ID='"+id+"'";
				st=connection.createStatement();
				rs=st.executeQuery(qs);
				while(rs.next()){
					foundClient.setCid(rs.getInt("Client_ID"));
					foundClient.setUid(rs.getInt("User_ID"));
					foundClient.setName(rs.getString("Name"));
					foundClient.setCnp(rs.getString("CNP"));
					foundClient.setAddress(rs.getString("Address"));
					foundClient.setCardid(rs.getString("CardId"));
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return foundClient;
	}
	
	//@Override
	public void updateClientName(ClientDB client, String newName) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Client SET Name='"+newName+"' WHERE Client_ID='"+client.getCid()+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + newName);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updateClientCNP(ClientDB client, String newCNP) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Client SET Name='"+newCNP+"' WHERE Client_ID='"+client.getCid()+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + newCNP);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updateClientAddress(ClientDB client, String newAddress) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Client SET Name='"+newAddress+"' WHERE Client_ID='"+client.getCid()+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + newAddress);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public ArrayList<ClientDB> findAllClients(int uid) {
		// TODO Auto-generated method stub
		ArrayList<ClientDB> listOfClients=new ArrayList<ClientDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM client where User_ID='"+uid+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				ClientDB clientFromList=new ClientDB();
				clientFromList.setCid(rs.getInt("Client_ID"));
				clientFromList.setUid(rs.getInt("User_ID"));
				clientFromList.setName(rs.getString("Name"));
				clientFromList.setCnp(rs.getString("CNP"));
				clientFromList.setAddress(rs.getString("Address"));
				clientFromList.setCardid(rs.getString("CardId"));
				System.out.println(clientFromList.toString());
				listOfClients.add(clientFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfClients;
	}
	
	public void updateClientCardId(ClientDB client, String newCardid) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Client SET Name='"+newCardid+"' WHERE Client_ID='"+client.getCid()+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + newCardid);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updateClient(int cid, ClientDB updatedData) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Client SET Name='"+updatedData.getName()+"', CNP='"+updatedData.getCnp()+"', Address='"+updatedData.getAddress()+"' WHERE CardId='"+updatedData.getCardid()+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data updated: "+updatedData.toString());
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	

}
