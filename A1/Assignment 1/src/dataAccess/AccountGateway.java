package dataAccess;

import java.sql.SQLException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class AccountGateway implements AccountDAO{
	

	Statement st=null;
	ResultSet rs=null;

	public AccountGateway()
	{
		
	}
	
	@Override
	public void insertAccount(int amount, String creation, String type, int aid, int cid) {
		try{
		Connection connection=ConnectionSingleton.getInstance().getConnection();
		String qs="INSERT INTO Account VALUES ('"+amount+"','"+creation+"','"+type+"','"+aid+"', '"+cid+"')";
		st=connection.createStatement();
		st.executeUpdate(qs);
		System.out.println("Data inserted: " + amount + " "+creation+" "+type );
	}catch (SQLException e){
		e.printStackTrace();
	}	
	}




	@Override
	public void updateAmount(int amount, int aid) {
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Account SET Amount='"+amount+"' WHERE Account_ID='"+aid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data changed: " + amount);
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<AccountDB> findAllAccounts(int cid) {
		// TODO Auto-generated method stub
		ArrayList<AccountDB> listOfAccounts=new ArrayList<AccountDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM account where Client_ID='"+cid+"'";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				AccountDB accountFromList=new AccountDB();
				accountFromList.setAid(rs.getInt("Account_ID"));
				accountFromList.setCid(rs.getInt("Client_ID"));
				accountFromList.setType(rs.getString("Type"));
				accountFromList.setAmount(rs.getInt("Amount"));
				accountFromList.setDateOfCreation(rs.getString("DateOfCreation"));
				System.out.println(accountFromList.toString());
				listOfAccounts.add(accountFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfAccounts;
	}
	
	public ArrayList<AccountDB> findAll() {
		// TODO Auto-generated method stub
		ArrayList<AccountDB> listOfAccounts=new ArrayList<AccountDB>();
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="SELECT * FROM account";
			st=connection.createStatement();
			rs=st.executeQuery(qs);
			while(rs.next()){
				AccountDB accountFromList=new AccountDB();
				accountFromList.setAid(rs.getInt("Account_ID"));
				accountFromList.setCid(rs.getInt("Client_ID"));
				accountFromList.setType(rs.getString("Type"));
				accountFromList.setAmount(rs.getInt("Amount"));
				accountFromList.setDateOfCreation(rs.getString("DateOfCreation"));
				System.out.println(accountFromList.toString());
				listOfAccounts.add(accountFromList);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listOfAccounts;
	}
	
	public AccountDB findAccountById(int id)
	{
		AccountDB foundAccount=new AccountDB();
			try{
				Connection connection=ConnectionSingleton.getInstance().getConnection();
				String qs="SELECT * FROM Account WHERE Account_ID='"+id+"'";
				st=connection.createStatement();
				rs=st.executeQuery(qs);
				while(rs.next()){
					foundAccount.setAid(rs.getInt("Account_ID"));
					foundAccount.setCid(rs.getInt("Client_ID"));
					foundAccount.setAmount(rs.getInt("Amount"));
					foundAccount.setDateOfCreation(rs.getString("DateOfCreation"));
					foundAccount.setType(rs.getString("Type"));
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return foundAccount;
	}

	@Override
	public void deleteAccount(int aid) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="DELETE FROM Account WHERE Account_ID='"+aid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data deleted: "+aid);
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updateAccount(int aid, AccountDB updatedData) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Account SET Amount='"+updatedData.getAmount()+"', DateOfCreation='"+updatedData.getDateOfCreation()+"', Type='"+updatedData.getType()+"' WHERE Account_ID='"+aid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data updated: "+updatedData.toString());
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	/*
	public void updateAmount(int aid, AccountDB updatedData) {
		// TODO Auto-generated method stub
		try{
			Connection connection=ConnectionSingleton.getInstance().getConnection();
			String qs="UPDATE Account SET Amount='"+updatedData.getAmount()+"', DateOfCreation='"+updatedData.getDateOfCreation()+"', Type='"+updatedData.getType()+"' WHERE Account_ID='"+aid+"'";
			st=connection.createStatement();
			st.executeUpdate(qs);
			System.out.println("Data updated: "+updatedData.toString());
		}catch (SQLException e){
			e.printStackTrace();
		}
	}*/

}
