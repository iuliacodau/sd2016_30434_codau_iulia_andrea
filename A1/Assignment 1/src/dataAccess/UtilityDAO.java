package dataAccess;

import java.sql.ResultSet;

public interface UtilityDAO {

	public void insertUtility(int uid, String type, int amount);
	public ResultSet findUtility(int uid);
	public void deleteUtility(int uid);
	public void updateType(String type, int uid);
	public void updateAmount(int amount, int uid);
	
}
