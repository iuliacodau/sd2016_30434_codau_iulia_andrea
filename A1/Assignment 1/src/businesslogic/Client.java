package businesslogic;

import java.util.ArrayList;
import java.util.regex.Pattern;

import dataAccess.ClientDB;
import dataAccess.ClientGateway;

public class Client {

	private int cid;
	private int uid;
	private String name;
	private String cnp;
	private String address;
	private String cardid;
	private final Pattern hasNumber=Pattern.compile("\\d");
	private final Pattern hasSpecialChar=Pattern.compile("[^a-zA-Z0-9 ]");
	private final Pattern hasUppercase=Pattern.compile("[A-Z]");
	private final Pattern hasLowercase=Pattern.compile("[a-z]");
	
	public Client(){
		
	}
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCnp() {
		return cnp;
	}
	public void setCnp(String cnp) {
		
		this.cnp = cnp;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}

	public boolean createClient(int uid, String name, String cnp, String address, String cardid){
		if (findId(cnp)==0)
		{
		this.uid=uid;
		this.name=name;
		this.cnp=cnp;
		this.address=address;
		this.cardid=cardid;
		ClientDB clientForDatabase=new ClientDB();
		clientForDatabase.setAddress(address);
		clientForDatabase.setCardid(cardid);
		clientForDatabase.setCid(cid);
		clientForDatabase.setCnp(cnp);
		clientForDatabase.setName(name);
		clientForDatabase.setUid(uid);
		ClientGateway clientGateway=new ClientGateway();
		clientGateway.insertClient(clientForDatabase);
		return true;
		} else {
			return false;
		}
	}

	public boolean checkName(String name){
		if (Character.isLowerCase(name.charAt(0)))
			return false;
		if (hasNumber.matcher(name).find())
			return false;
		if (hasSpecialChar.matcher(name).find())
			return false;
		return true;
	}
	
	public boolean checkCNP(String cnp) {
		if (cnp.length()!=13)
			return false;
		if (hasLowercase.matcher(cnp).find())
			return false;
		if (hasUppercase.matcher(cnp).find())
			return false;
		if (hasSpecialChar.matcher(cnp).find())
			return false;
		if ((cnp.charAt(0)!='1') && (cnp.charAt(0)!='2'))
			return false;
		if ((cnp.charAt(3)!='0') && (cnp.charAt(3)!='1') && (cnp.charAt(3)!='2'))
			return false;
		if ((cnp.charAt(5)!='0') && (cnp.charAt(5)!='1') && (cnp.charAt(5)!='2') && (cnp.charAt(5)!='3'))
			return false;
		return true;
	}
	
	public boolean checkId(String id){
		if (hasSpecialChar.matcher(id).find())
			return false;
		if (hasLowercase.matcher(id).find())
			return false;
		return true;
	}
	
	public Client findClientByName(String name) {
		ClientGateway clientGateway=new ClientGateway();
		ClientDB clientFromDatabase=clientGateway.findClientByName(name);
		this.address=clientFromDatabase.getAddress();
		this.cardid=clientFromDatabase.getCardid();
		this.cid=clientFromDatabase.getCid();
		this.cnp=clientFromDatabase.getCnp();
		this.name=clientFromDatabase.getName();
		this.uid=clientFromDatabase.getUid();
		return this;
	} 
	
	public Client findClientByCnp(String cnp) {
		ClientGateway clientGateway=new ClientGateway();
		ClientDB clientFromDatabase=clientGateway.findClientByCnp(cnp);
		this.address=clientFromDatabase.getAddress();
		this.cardid=clientFromDatabase.getCardid();
		this.cid=clientFromDatabase.getCid();
		this.cnp=clientFromDatabase.getCnp();
		this.name=clientFromDatabase.getName();
		this.uid=clientFromDatabase.getUid();
		return this;
	}
	
	public void deleteClient(int clientId){
		ClientGateway clientGateway=new ClientGateway();
		clientGateway.deleteClient(clientId);
		
	}
	
	public int findId(String cnp) {
		ClientDB temp=new ClientDB();
		ClientGateway tempg=new ClientGateway();
		temp=tempg.findClientByCnp(cnp);
		return temp.getCid();
	}
	
	@Override
	public String toString() {
		return "Client [cid=" + cid + ", uid=" + uid + ", name=" + name + ", cnp=" + cnp + ", address=" + address
				+ ", cardid=" + cardid + "]";
	}
	
	public ArrayList<Client> findAllClients(int uid) {
		ClientGateway temporaryClient=new ClientGateway();
		ArrayList<Client> allClients=new ArrayList<Client>();
		ArrayList<ClientDB> allClientsFromDatabase=new ArrayList<ClientDB>();
		allClientsFromDatabase=temporaryClient.findAllClients(uid);
		System.out.println(allClientsFromDatabase.size());
		for (int i=0; i<allClientsFromDatabase.size(); i++)
		{
			System.out.println("Here");
			Client addedClient=new Client();
			addedClient.setCid(allClientsFromDatabase.get(i).getCid());
			addedClient.setUid(allClientsFromDatabase.get(i).getUid());
			addedClient.setName(allClientsFromDatabase.get(i).getName());
			addedClient.setCnp(allClientsFromDatabase.get(i).getCnp());
			addedClient.setAddress(allClientsFromDatabase.get(i).getAddress());
			addedClient.setCardid(allClientsFromDatabase.get(i).getCardid());
			System.out.println("The "+addedClient.toString());
			allClients.add(addedClient);
		}
		return allClients;
	}
	
	public boolean updateClient(int cid, int uid, String name, String cnp, String address, String cardid)
	{
		this.uid=uid;
		this.cid=cid;
		this.name=name;
		this.cnp=cnp;
		this.address=address;
		this.cardid=cardid;
		ClientDB updatedData=new ClientDB();
		ClientGateway clientGateway=new ClientGateway();
		updatedData.setCid(cid);
		updatedData.setAddress(address);
		updatedData.setCardid(cardid);
		updatedData.setCid(uid);
		updatedData.setCnp(cnp);
		updatedData.setName(name);
		clientGateway.updateClient(cid, updatedData);
		return true;
	}
	
	public void deleteClient() {
		ClientGateway temp=new ClientGateway();
		temp.deleteClient(this.getCid());
	}
}
