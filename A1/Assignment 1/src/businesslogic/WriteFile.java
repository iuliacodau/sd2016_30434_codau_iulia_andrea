package businesslogic;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class WriteFile {

	private String path;
	private boolean appendToFile=false;
	
	public WriteFile(String path){
		this.path=path;
	}
	
	public WriteFile(String path, boolean appendToFile){
		this.path=path;
		this.appendToFile=appendToFile;
	}
	
	public void writeToFile(String textLine) throws IOException{
		FileWriter write=new FileWriter(path,appendToFile);
		PrintWriter printLine=new PrintWriter(write);
		printLine.printf("%s", textLine);
		printLine.println();
		printLine.flush();
		printLine.close();
	}
}
