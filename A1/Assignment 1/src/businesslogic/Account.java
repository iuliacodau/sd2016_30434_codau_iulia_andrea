package businesslogic;

import java.util.ArrayList;
import java.util.regex.Pattern;

import dataAccess.AccountDB;
import dataAccess.AccountGateway;

public class Account {
	
	private int amount;
	private String creation;
	private String type;
	private int aid;
	private int cid;
	private AccountGateway ag = new AccountGateway();
	private boolean negativeSumRetraction=false;
	private boolean notEnoughMoney=false;
	
	public Account()
	{
		
	}
	
	@Override
	public String toString() {
		return "Account [amount=" + amount + ", creation=" + creation + ", type=" + type + ", aid=" + aid + ", cid="
				+ cid + "]";
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCreation() {
		return creation;
	}

	public void setCreation(String creation) {
		this.creation = creation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}
	
	public boolean addSum(int sum, int aid)
	{
		if (sum>0)
		{
			this.amount=amount+sum;	
			ag.updateAmount(amount, aid);
			return true;
		}
		else 
		{
			System.out.println("Negative sum");
			return false;
		}
	}
	
	public void retractSum(int sum, int aid)
	{
		if (sum>0) {
			if (this.amount-sum>0){
				this.amount=amount-sum;	
				ag.updateAmount(amount, aid);
				negativeSumRetraction=false;
			}else {
				notEnoughMoney=true;
			}
		} else 
			negativeSumRetraction=true;
	}
	
	public boolean getError1()
	{
		return negativeSumRetraction;
	}
	
	public boolean getError2(){
		return notEnoughMoney;
	}
	
	public void createAccount(int amount, String creation, String type, int aid, int cid)
	{
		this.amount=amount;
		this.creation=creation;
		this.type=type;
		this.aid=aid;
		this.cid=cid;
		ag.insertAccount(amount, creation, type, aid, cid);
	}
	
	public ArrayList<Account> findAllAccounts(int cid) {
		AccountGateway temporaryAccount=new AccountGateway();
		ArrayList<Account> allAccounts=new ArrayList<Account>();
		ArrayList<AccountDB> allAccountsFromDatabase=new ArrayList<AccountDB>();
		allAccountsFromDatabase=temporaryAccount.findAllAccounts(cid);
		System.out.println(allAccountsFromDatabase.size());
		for (int i=0; i<allAccountsFromDatabase.size(); i++)
		{
			System.out.println("Here");
			Account addedAccount=new Account();
			addedAccount.setAid(allAccountsFromDatabase.get(i).getAid());
			addedAccount.setCid(allAccountsFromDatabase.get(i).getCid());
			addedAccount.setAmount(allAccountsFromDatabase.get(i).getAmount());
			addedAccount.setCreation(allAccountsFromDatabase.get(i).getDateOfCreation());
			addedAccount.setType(allAccountsFromDatabase.get(i).getType());
			System.out.println("The "+addedAccount.toString());
			allAccounts.add(addedAccount);
		}
		return allAccounts;
	}
	
	public ArrayList<Account> findAll() {
		AccountGateway temporaryAccount=new AccountGateway();
		ArrayList<Account> allAccounts=new ArrayList<Account>();
		ArrayList<AccountDB> allAccountsFromDatabase=new ArrayList<AccountDB>();
		allAccountsFromDatabase=temporaryAccount.findAll();
		System.out.println(allAccountsFromDatabase.size());
		for (int i=0; i<allAccountsFromDatabase.size(); i++)
		{
			System.out.println("Here");
			Account addedAccount=new Account();
			addedAccount.setAid(allAccountsFromDatabase.get(i).getAid());
			addedAccount.setCid(allAccountsFromDatabase.get(i).getCid());
			addedAccount.setAmount(allAccountsFromDatabase.get(i).getAmount());
			addedAccount.setCreation(allAccountsFromDatabase.get(i).getDateOfCreation());
			addedAccount.setType(allAccountsFromDatabase.get(i).getType());
			System.out.println("The "+addedAccount.toString());
			allAccounts.add(addedAccount);
		}
		return allAccounts;
	}
	
	public Account findAccountById(int id) {
		AccountGateway accountGateway=new AccountGateway();
		AccountDB accountFromDatabase=accountGateway.findAccountById(id);
		this.aid=accountFromDatabase.getAid();
		this.cid=accountFromDatabase.getCid();
		this.amount=accountFromDatabase.getAmount();
		this.creation=accountFromDatabase.getDateOfCreation();
		this.type=accountFromDatabase.getType();
		return this;
	} 
	
	public void deleteAccount(int aid){
		AccountGateway accountGateway=new AccountGateway();
		accountGateway.deleteAccount(aid);
		
	}
	
	public boolean updateAccount(int amount, String doc, String type, int aid, int cid)
	{
		if (amount>0) {
			this.amount=amount;
			this.type=type;
			this.creation=doc;
			AccountDB updatedData=new AccountDB();
			updatedData.setAmount(amount);
			updatedData.setDateOfCreation(doc);
			updatedData.setType(type);
			updatedData.setAid(aid);
			updatedData.setCid(cid);
			ag.updateAccount(aid, updatedData);
			return true;
		}
		else return false;
	}
	
	public boolean addSum(int extra, int amount, String doc, String type, int aid, int cid)
	{
		if (amount+extra>0) {
			this.amount=amount+extra;
			this.type=type;
			this.creation=doc;
			AccountDB updatedData=new AccountDB();
			updatedData.setAmount(amount);
			updatedData.setDateOfCreation(doc);
			updatedData.setType(type);
			updatedData.setAid(aid);
			updatedData.setCid(cid);
			ag.updateAccount(aid, updatedData);
			return true;
		}
		else return false;
	}
	
}
