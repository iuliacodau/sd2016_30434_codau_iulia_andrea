package businesslogic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import dataAccess.UserDB;
import dataAccess.UserGateway;

public class User {

	private int uid;
	private String type;
	private String username;
	private String password;
	private UserGateway ug=new UserGateway();
	private boolean passwordError=false;
	private final Pattern hasUppercase=Pattern.compile("[A-Z]");
	private final Pattern hasLowercase=Pattern.compile("[a-z]");
	private final Pattern hasNumber=Pattern.compile("\\d");
	private final Pattern hasSpecialChar=Pattern.compile("[^a-zA-Z0-9 ]");
	
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		if (validPassword(password)==true)
			this.password = password;
		else 
		{
			passwordError=true;
			System.out.println("Password missing uppercase...");
		}
	}
	
	public boolean createUser(int uid, String type, String username, String password)
	{
		if (findId(username)==0) {
			this.type=type;
			this.username=username;
			this.password=password;
			ug.insertUser(type, username, password);
			//this.uid=ug.findId(username);
			this.uid=ug.findUserByUsername(username).getUid();
			return true;
		}
		else {
			System.out.println("The username already exists");
			return false;
		}
		
	}
	
	public void deleteUser() {
		ug.deleteUser(this.getUid());
	}
	
	public boolean updateUser(int uid, String type, String username, String password)
	{
		if ((findId(username)==0)||(findId(username)==uid)) {
			this.uid=uid;
			this.type=type;
			this.username=username;
			this.password=password;
			UserDB updatedData=new UserDB();
			updatedData.setPassword(password);
			updatedData.setType(type);
			updatedData.setUid(uid);
			updatedData.setUsername(username);
			ug.updateUser(uid, updatedData);
			return true;
		}
		else return false;
	}
	
	public boolean validPassword(String password){
		if (password==null)
			return false;
		if (password.length()<8)
			return false;
		if (!hasUppercase.matcher(password).find())
			return false;
		if (!hasLowercase.matcher(password).find())
			return false;
		if (!hasNumber.matcher(password).find())
			return false;
		if (!hasSpecialChar.matcher(password).find())
			return false;
		return true;
	}
	
	public boolean checkPassword(String passwordGiven, String username)
	{
		UserDB inputUser=new UserDB();
		UserGateway userGateway=new UserGateway();
		inputUser=userGateway.findUserByUsername(username);
		if (inputUser.getPassword().equals(passwordGiven))
			return true;
		else{
			System.out.println("Password doesn't match user");
			return false;
			}
	}
	
	public boolean isAdmin()
	{
		System.out.println("Type is: "+type);
		if (type.equals("admin"))
			return true;
		return false;
	}

	public User findUserByUsername(String username) {
		UserGateway userGateway=new UserGateway();
		UserDB userFromDatabase=userGateway.findUserByUsername(username);
		this.uid=userFromDatabase.getUid();
		this.type=userFromDatabase.getType();
		this.username=userFromDatabase.getUsername();
		this.password=userFromDatabase.getPassword();
		return this;
	} 
	
	public ArrayList<User> findAllUsers() {
		UserGateway temporaryUser=new UserGateway();
		ArrayList<User> allUsers=new ArrayList<User>();
		ArrayList<UserDB> allUsersFromDatabase=new ArrayList<UserDB>();
		allUsersFromDatabase=temporaryUser.findAllUsers();
		for (int i=0; i<allUsersFromDatabase.size(); i++)
		{
			User addUser=new User();
			addUser.setUid(allUsersFromDatabase.get(i).getUid());
			addUser.setType(allUsersFromDatabase.get(i).getType());
			addUser.setUsername(allUsersFromDatabase.get(i).getUsername());
			addUser.setPassword(allUsersFromDatabase.get(i).getPassword());
			allUsers.add(addUser);
		}
		return allUsers;
	}
	
	@Override
	public String toString() {
		return "User [uid=" + uid + ", type=" + type + ", username=" + username + ", password=" + password + "]";
	} 
	
	public int findId(String username) {
		UserDB temp=new UserDB();
		UserGateway tempg=new UserGateway();
		temp=tempg.findUserByUsername(username);
		return temp.getUid();
	}

	
}
