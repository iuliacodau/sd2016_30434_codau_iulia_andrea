package businesslogic;

import java.text.ParseException;
import java.util.ArrayList;

import dataAccess.ReportDB;
import dataAccess.ReportGateway;

public class Report {

	private int rid;
	private int uid;
	private String date;
	private String type;
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date2) {
		this.date = date2;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Report [rid=" + rid + ", uid=" + uid + ", date=" + date + ", type=" + type + "]";
	}
	
	public ArrayList<Report> findAllReports(int uid, String inputStr,  String inputStr1 ) throws ParseException {	
		ReportGateway temporaryReport=new ReportGateway();
		ArrayList<Report> allReports=new ArrayList<Report>();
		ArrayList<ReportDB> allReportsFromDatabase=new ArrayList<ReportDB>();
		allReportsFromDatabase=temporaryReport.findReportRange(uid, inputStr, inputStr1);
		System.out.println(allReportsFromDatabase.size());
		for (int i=0; i<allReportsFromDatabase.size(); i++)
		{
			Report addedReport=new Report();
			addedReport.setRid(allReportsFromDatabase.get(i).getRid());
			addedReport.setUid(allReportsFromDatabase.get(i).getUid());
			addedReport.setType(allReportsFromDatabase.get(i).getType());
			addedReport.setDate(allReportsFromDatabase.get(i).getDoa());
			allReports.add(addedReport);
		}
		return allReports;
	}
	
	public void createReport(int uid, String date, String type) throws ParseException{
		this.uid=uid;
		this.type=type;
		this.type=type;
		ReportDB reportForDatabase=new ReportDB();
		reportForDatabase.setDoa(date);
		reportForDatabase.setUid(uid);
		reportForDatabase.setType(type);
		ReportGateway reportGateway=new ReportGateway();
		reportGateway.insertReport(reportForDatabase);
	}
	
	public ArrayList<Report> findAllReports(int uid) {
		ReportGateway temporaryReport=new ReportGateway();
		ArrayList<Report> allReports=new ArrayList<Report>();
		ArrayList<ReportDB> allReportsFromDatabase=new ArrayList<ReportDB>();
		allReportsFromDatabase=temporaryReport.findAllReports(uid);
		System.out.println(allReportsFromDatabase.size());
		for (int i=0; i<allReportsFromDatabase.size(); i++)
		{
			System.out.println("Here");
			Report addedReport=new Report();
			addedReport.setRid(allReportsFromDatabase.get(i).getRid());
			addedReport.setType(allReportsFromDatabase.get(i).getType());
			addedReport.setDate(allReportsFromDatabase.get(i).getDoa());
			addedReport.setUid(allReportsFromDatabase.get(i).getUid());
			System.out.println("The "+addedReport.toString());
			allReports.add(addedReport);
		}
		return allReports;
	}
}
