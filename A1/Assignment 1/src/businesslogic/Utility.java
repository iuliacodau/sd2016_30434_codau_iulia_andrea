package businesslogic;

import java.util.ArrayList;

import dataAccess.UtilityDB;
import dataAccess.UtilityGateway;

public class Utility {

	private int uid;
	private String type;
	private int amount;
	private int cid;
	private UtilityGateway utilityGateway;
	
	
	
	@Override
	public String toString() {
		return "Utility [uid=" + uid + ", type=" + type + ", amount=" + amount + ", cid=" + cid + "]";
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getUid() {

		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public void payUtility(){
		this.amount=0;
	}
	
	public void createUtility(int uid, String type, int amount){
		this.uid=uid;
		this.type=type;
		this.amount=amount;
		utilityGateway.insertUtility(uid, type, amount);
	}
	
	public ArrayList<Utility> findAllUtilities(int cid) {
		UtilityGateway temporaryUtility=new UtilityGateway();
		ArrayList<Utility> allUtilities=new ArrayList<Utility>();
		ArrayList<UtilityDB> allUtilitiesFromDatabase=new ArrayList<UtilityDB>();
		allUtilitiesFromDatabase=temporaryUtility.findAllUtilities(cid);
		System.out.println(allUtilitiesFromDatabase.size());
		for (int i=0; i<allUtilitiesFromDatabase.size(); i++)
		{
			System.out.println("Here");
			Utility addedUtility=new Utility();
			addedUtility.setUid(allUtilitiesFromDatabase.get(i).getUid());
			addedUtility.setCid(allUtilitiesFromDatabase.get(i).getCid());
			addedUtility.setAmount(allUtilitiesFromDatabase.get(i).getAmount());
			addedUtility.setType(allUtilitiesFromDatabase.get(i).getType());
			System.out.println("The "+addedUtility.toString());
			allUtilities.add(addedUtility);
		}
		return allUtilities;
	}
	
	public Utility findUtilityById(int id) {
		UtilityGateway utilityGateway=new UtilityGateway();
		UtilityDB utilityFromDatabase=utilityGateway.findUtilityById(id);
		this.uid=utilityFromDatabase.getUid();
		this.cid=utilityFromDatabase.getCid();
		this.amount=utilityFromDatabase.getAmount();
		this.type=utilityFromDatabase.getType();
		return this;
	} 
	
	public void deleteUtility(int uid){
		UtilityGateway utilityGateway=new UtilityGateway();
		utilityGateway.deleteUtility(uid);
		
	}
}
