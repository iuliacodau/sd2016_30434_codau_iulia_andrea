package presentation;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businesslogic.Account;
import businesslogic.Client;
import businesslogic.Report;
import businesslogic.User;
import businesslogic.Utility;

import javax.swing.JTextArea;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JRadioButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JSeparator;

public class Clients2 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private ArrayList<Account> listOfAccounts=new ArrayList<Account>();
	private ArrayList<Account> listOfAccounts_1=new ArrayList<Account>();
	private ArrayList<Utility> listOfUtilities=new ArrayList<Utility>();
	private Utility utility=new Utility();
	private Account account=new Account();
	private JTextField textField_1;
    private boolean spendingChecked=false;
    private boolean savingChecked=false;
	private final Pattern hasSpecialChar=Pattern.compile("[^-a-zA-Z0-9 ]");
	private final Pattern hasLetter=Pattern.compile("[A-Za-z]");

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clients2 frame = new Clients2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Clients2(final Client c, final User u) {
		
		final Report newReport=new Report();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 712);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(124, 155, 206, 51);
		textArea.setLineWrap(true);
		contentPane.add(textArea);
		
		
		final JTextPane textPane_2 = new JTextPane();
		textPane_2.setBounds(341, 542, 45, 23);
		contentPane.add(textPane_2);
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(124, 124, 99, 20);
		
		account.setCid(c.getCid());
		listOfAccounts=account.findAllAccounts(c.getCid());
		System.out.println(" ewfrtehr "+ listOfAccounts.size());
		for (int i=0; i<listOfAccounts.size();i++){
			comboBox.addItem(""+listOfAccounts.get(i).getAid());
			//textArea.setText(""+listOfAccounts.get(i).getAmount());
		}
		
		System.out.println("This button is clicekd "+comboBox.getSelectedItem());
		
		
		

		contentPane.add(comboBox);
		
		JLabel lblClientAccount = new JLabel("Client Account");
		lblClientAccount.setForeground(new Color(255, 255, 255));
		lblClientAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		lblClientAccount.setBounds(132, 38, 222, 33);
		contentPane.add(lblClientAccount);
		
		JLabel lblSelectAccount = new JLabel("Transfers");
		lblSelectAccount.setForeground(new Color(255, 255, 255));
		lblSelectAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblSelectAccount.setBounds(300, 355, 206, 14);
		contentPane.add(lblSelectAccount);
		
		textField = new JTextField();
		textField.setBounds(107, 397, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		final JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setBounds(360, 397, 96, 20);
		
		listOfAccounts_1=account.findAll();
		System.out.println(" ewfrtehr "+ listOfAccounts_1.size());
		for (int i=0; i<listOfAccounts_1.size();i++){
			comboBox_1.addItem(""+listOfAccounts_1.get(i).getAid());
			//textArea.setText(""+listOfAccounts.get(i).getAmount());
		}
		
		
		contentPane.add(comboBox_1);
		
		final JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.setBounds(216, 544, 105, 20);
		
		listOfUtilities=utility.findAllUtilities(c.getCid());
	//	System.out.println(" ewfrtehr "+ listOfAccounts_1.size());
		for (int i=0; i<listOfUtilities.size();i++){
			comboBox_2.addItem(""+listOfUtilities.get(i).getUid());
			//textPane_2.setText(""+listOfUtilities.get(i).getAmount());

			//textArea.setText(""+listOfAccounts.get(i).getAmount());
		}
		
		contentPane.add(comboBox_2);
		
		JLabel lblEnterAmount = new JLabel("Amount");
		lblEnterAmount.setForeground(new Color(255, 255, 255));
		lblEnterAmount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblEnterAmount.setBounds(13, 400, 142, 14);
		contentPane.add(lblEnterAmount);
		
		JLabel lblCheckAccount = new JLabel("Check Account");
		lblCheckAccount.setForeground(new Color(255, 255, 255));
		lblCheckAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblCheckAccount.setBounds(144, 82, 149, 30);
		contentPane.add(lblCheckAccount);
		
		JLabel lblTransactions = new JLabel("Transactions");
		lblTransactions.setForeground(new Color(255, 255, 255));
		lblTransactions.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblTransactions.setBounds(37, 355, 126, 14);
		contentPane.add(lblTransactions);
		
		JButton btnCreateNewAccount = new JButton("Create New Account");
		btnCreateNewAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NewAccount newAccountWindow=new NewAccount(c,u);
				newAccountWindow.setVisible(true);
			}
		});
		btnCreateNewAccount.setBounds(154, 251, 149, 23);
		contentPane.add(btnCreateNewAccount);
		
		JButton btnDeleteAccount = new JButton("Delete Account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Account deletedAcc=new Account();
				deletedAcc.deleteAccount(Integer.parseInt((String) comboBox.getSelectedItem()));
				Date DateOfToday=new Date();
				DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
				String today=df.format(DateOfToday);
				try {
					newReport.createReport(u.getUid(),today , "Account deleted : "+ Integer.parseInt((String) comboBox.getSelectedItem()));
				} catch (ParseException exc) {
					// TODO Auto-generated catch block
					exc.printStackTrace();
				}
				textArea.setText(null);
				comboBox.removeItem(comboBox.getSelectedItem());
				comboBox_1.removeItem(comboBox_1.getSelectedItem());
				repaint();
			}
		});
		btnDeleteAccount.setBounds(154, 217, 149, 23);
		contentPane.add(btnDeleteAccount);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(null);
				if (textField.getText().isEmpty()){
					JOptionPane.showMessageDialog(contentPane, "Empty field");
				}else
					if (hasLetter.matcher(textField.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField.setText(null);
					}
					else if (hasSpecialChar.matcher(textField.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField.setText(null);
					}
				else 
				{	
				int newSum=Integer.parseInt(textField.getText());
				Account newAcc=new Account();
				newAcc.findAccountById(Integer.parseInt((String) comboBox.getSelectedItem()));
				boolean noError=newAcc.addSum(newSum, newAcc.getAid());
				if (!noError)
				{
					JOptionPane.showMessageDialog(contentPane, "Cannot introduce negative sum");
					textField.setText(null);
				} else {
					Date DateOfToday=new Date();
					DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
					String today=df.format(DateOfToday);
					try {
						newReport.createReport(u.getUid(),today , "Sum "+newSum+ " added to account "+newAcc.getAid());
					} catch (ParseException exc) {
						// TODO Auto-generated catch block
						exc.printStackTrace();
					}
				}
			}
			}
		});
		btnAdd.setBounds(48, 428, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnRetrieve = new JButton("Retrieve");
		btnRetrieve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText(null);
				if (textField.getText().isEmpty()){
					JOptionPane.showMessageDialog(contentPane, "Empty amount field");
				}
				else
					if (hasLetter.matcher(textField.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField.setText(null);
					}
					else if (hasSpecialChar.matcher(textField.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField.setText(null);
					}
				else{
				int newSum=Integer.parseInt(textField.getText());
				Account newAcc=new Account();
				newAcc.findAccountById(Integer.parseInt((String) comboBox.getSelectedItem()));
				newAcc.retractSum(newSum, newAcc.getAid());
				if (newAcc.getError1()) {
					JOptionPane.showMessageDialog(contentPane, "Cannot introduce negative sum");
					textField.setText(null);
				}
				else if (newAcc.getError2()) {
					JOptionPane.showMessageDialog(contentPane, "Insufficient funds");
					textField.setText(null);
				} else {
					Date DateOfToday=new Date();
					DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
					String today=df.format(DateOfToday);
					try {
						newReport.createReport(u.getUid(),today , "Sum "+newSum+ " retrieved from account "+newAcc.getAid());
					} catch (ParseException exc) {
						// TODO Auto-generated catch block
						exc.printStackTrace();
					}
				}
			}
			}
		});
		btnRetrieve.setBounds(48, 462, 89, 23);
		contentPane.add(btnRetrieve);
		
		JButton btnTransfer = new JButton("Transfer");
		btnTransfer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText(null);
				if (textField_1.getText().isEmpty()){
					JOptionPane.showMessageDialog(contentPane, "Empty Amount Field");
				} 
				else
					if (hasLetter.matcher(textField_1.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField_1.setText(null);
					}
					else if (hasSpecialChar.matcher(textField_1.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField_1.setText(null);
					}
				else {
				int newSum=Integer.parseInt(textField_1.getText());
				Account fromAcc=new Account();
				fromAcc.findAccountById(Integer.parseInt((String) comboBox.getSelectedItem()));
				Account toAcc=new Account();
				toAcc.findAccountById(Integer.parseInt((String) comboBox_1.getSelectedItem()));
				if (fromAcc.getAid()!=toAcc.getAid()){
				fromAcc.retractSum(newSum, fromAcc.getAid());
				if (fromAcc.getError1()) {
					JOptionPane.showMessageDialog(contentPane, "Cannot introduce negative sum");
					textField_1.setText(null);
				}
				else if (fromAcc.getError2()) {
					JOptionPane.showMessageDialog(contentPane, "Insufficient funds");
					textField_1.setText(null);
				} else {
				boolean noError=toAcc.addSum(newSum, toAcc.getAid());
				if (!noError)
				{
					JOptionPane.showMessageDialog(contentPane, "Cannot introduce negative sum");
					textField_1.setText(null);
				}
				}
				
				Date DateOfToday=new Date();
				DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
				String today=df.format(DateOfToday);
				try {
					newReport.createReport(u.getUid(),today , "Sum "+newSum+ " transfered from "+fromAcc.getAid() +" to "+toAcc.getAid());
				} catch (ParseException exc) {
					// TODO Auto-generated catch block
					exc.printStackTrace();
				}
				
			}
			 else {
				JOptionPane.showMessageDialog(contentPane, "Cannot transfer to the same account");
				textField_1.setText(null);
			}
			}
				
			}
		});
		btnTransfer.setBounds(322, 462, 89, 23);
		contentPane.add(btnTransfer);
		
		JLabel lblTransferTo = new JLabel("Transfer to");
		lblTransferTo.setForeground(new Color(255, 255, 255));
		lblTransferTo.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblTransferTo.setBounds(243, 398, 126, 14);
		contentPane.add(lblTransferTo);
		
		final JTextPane textPane = new JTextPane();
		textPane.setBounds(361, 17, 89, 23);
		textPane.setText("Client ID: "+c.getCid());
		textPane.setEditable(false);
		contentPane.add(textPane);
		
		JButton btnGo = new JButton("Go!");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Account newAcc=new Account();
				newAcc.findAccountById(Integer.parseInt((String) comboBox.getSelectedItem()));
				textArea.setText(newAcc.toString());
			}
		});
		btnGo.setBounds(241, 123, 89, 23);
		contentPane.add(btnGo);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Home logout=new Home();
				logout.getFrame().setVisible(true);
				dispose();
				
			}
		});
		btnLogout.setBounds(23, 51, 99, 23);
		contentPane.add(btnLogout);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBounds(23, 17, 99, 23);
		textPane_1.setText(u.getUsername());
		textPane_1.setEditable(false);
		contentPane.add(textPane_1);
		
		JLabel lblUtilities = new JLabel("Utilities");
		lblUtilities.setForeground(new Color(255, 255, 255));
		lblUtilities.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblUtilities.setBounds(175, 509, 105, 24);
		contentPane.add(lblUtilities);
		

		
		JLabel lblSelectUtility = new JLabel("Select Utility");
		lblSelectUtility.setForeground(new Color(255, 255, 255));
		lblSelectUtility.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblSelectUtility.setBounds(64, 547, 142, 14);
		contentPane.add(lblSelectUtility);

		
		final JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(106, 622, 231, 41);
		textArea_1.setLineWrap(true);
		contentPane.add(textArea_1);
		
		JButton btnPay = new JButton("Pay!");
		btnPay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textPane_2.getText().isEmpty()) {
					JOptionPane.showMessageDialog(contentPane, "Select utility");
				} else {
				int bill=Integer.parseInt((String) textPane_2.getText());
				Account fromAcc=new Account();
				fromAcc.findAccountById(Integer.parseInt((String) comboBox.getSelectedItem()));
				fromAcc.retractSum(bill, fromAcc.getAid());
				if (fromAcc.getError2()) {
					JOptionPane.showMessageDialog(contentPane, "Insufficient funds");
				} else {
					JOptionPane.showMessageDialog(contentPane, "Bill paid");
					
					Date DateOfToday=new Date();
					DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
					String today=df.format(DateOfToday);
					try {
						newReport.createReport(u.getUid(),today , "Paid "+bill+" for utility");
					} catch (ParseException exc) {
						// TODO Auto-generated catch block
						exc.printStackTrace();
					}
					
					Utility ut=new Utility();
					ut.deleteUtility(Integer.parseInt((String) comboBox_2.getSelectedItem()));
					comboBox_2.removeItem(comboBox_2.getSelectedItem());
					textArea.setText(null);
					textArea_1.setText(null);
					textPane_2.setText(null);
				}
				}
			}
		});
		btnPay.setBounds(211, 575, 89, 23);
		contentPane.add(btnPay);

		
		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Utility selectedUtility=new Utility();
				if (comboBox_2.getSelectedItem()==null){
					JOptionPane.showMessageDialog(contentPane, "Please select utility");
				} else {
				selectedUtility.findUtilityById(Integer.parseInt((String) comboBox_2.getSelectedItem()));
				textArea_1.setText(selectedUtility.toString());
				textPane_2.setText(""+selectedUtility.getAmount());
				}
			}
		});
		btnSelect.setBounds(106, 575, 89, 23);
		contentPane.add(btnSelect);
		
		JRadioButton rdbtnSpendingAccount = new JRadioButton("Spending");
		rdbtnSpendingAccount.setBounds(93, 285, 111, 23);
		contentPane.add(rdbtnSpendingAccount);
		
		JRadioButton rdbtnSavingsAccount = new JRadioButton("Savings");
		rdbtnSavingsAccount.setBounds(93, 311, 111, 23);
		contentPane.add(rdbtnSavingsAccount);
		
		final ButtonGroup group = new ButtonGroup();
	    group.add(rdbtnSpendingAccount);
	    group.add(rdbtnSavingsAccount);
	    
	    rdbtnSpendingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             spendingChecked=true;
	             savingChecked=false;
	          }           
	       });

	    rdbtnSavingsAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
				spendingChecked=false;
				savingChecked=true;
	          }           
	       });
		
		JButton btnUpdateAccount = new JButton("Update Account");
		btnUpdateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Account newAcc=new Account();
				newAcc.findAccountById(Integer.parseInt((String) comboBox.getSelectedItem()));
				System.out.println("IS SPENDING "+spendingChecked);
				System.out.println("OR"+savingChecked);
				if (spendingChecked) {
					//newAcc.updateAccount(newAcc.getAmount(), newAcc.getCreation(), "spending", newAcc.getAid(), newAcc.getCid());
					newAcc.setType("spending");
					//spendingChecked=false;
					//savingChecked=false;
				}
				else if (savingChecked) {
					//newAcc.updateAccount(newAcc.getAmount(), newAcc.getCreation(), "saving", newAcc.getAid(), newAcc.getCid());
					newAcc.setType("saving");
					//spendingChecked=false;
					//savingChecked=false;
				}
				boolean sm=newAcc.updateAccount(newAcc.getAmount(), newAcc.getCreation(), newAcc.getType(), newAcc.getAid(), newAcc.getCid());
				Date DateOfToday=new Date();
				DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
				String today=df.format(DateOfToday);
				try {
					newReport.createReport(u.getUid(),today , "Account "+newAcc.getAid()+" updated to "+ newAcc.getType());
				} catch (ParseException exc) {
					// TODO Auto-generated catch block
					exc.printStackTrace();
				}
				textArea.setText(null);
				group.clearSelection();
			}
		});
		btnUpdateAccount.setBounds(231, 298, 133, 23);
		contentPane.add(btnUpdateAccount);
		
		JButton btnUpdateClient = new JButton("Update");
		btnUpdateClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateClient updateWindow=new UpdateClient(c,u);
				updateWindow.setVisible(true);
			}
		});
		btnUpdateClient.setBounds(361, 51, 89, 23);
		contentPane.add(btnUpdateClient);


		
		JSeparator separator = new JSeparator();
		separator.setBounds(20, 85, 102, 9);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(23, 7, 99, 14);
		contentPane.add(separator_1);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setForeground(Color.WHITE);
		lblAmount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblAmount.setBounds(242, 430, 105, 14);
		contentPane.add(lblAmount);
		
		textField_1 = new JTextField();
		textField_1.setBounds(370, 429, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(13, 341, 440, 9);
		contentPane.add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(13, 496, 440, 9);
		contentPane.add(separator_3);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(354, 7, 99, 14);
		contentPane.add(separator_4);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(357, 80, 99, 14);
		contentPane.add(separator_5);
		

	}
}
