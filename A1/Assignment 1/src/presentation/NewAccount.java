package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businesslogic.Account;
import businesslogic.Client;
import businesslogic.Report;
import businesslogic.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.Font;

public class NewAccount extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private boolean savingChecked=false;
	private boolean spendingChecked=false;
	private final Pattern hasSpecialChar=Pattern.compile("[^a-zA-Z0-9 ]");
	private final Pattern hasLetter=Pattern.compile("[A-Za-z]");
	
	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client somebody=new Client();
					somebody.setCid(3);
					NewAccount frame = new NewAccount(somebody, new Clients2().getContentPane());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public NewAccount(final Client client, final User user) {
		
		final int c_id=client.getCid();
		final Report newReport=new Report();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 325);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCreateNewAccount = new JLabel("Create New Account");
		lblCreateNewAccount.setForeground(new Color(255, 255, 255));
		lblCreateNewAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		lblCreateNewAccount.setBounds(111, 33, 279, 38);
		contentPane.add(lblCreateNewAccount);
		
		JLabel lblDate = new JLabel("Date :");
		lblDate.setForeground(new Color(255, 255, 255));
		lblDate.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblDate.setBounds(10, 93, 98, 14);
		contentPane.add(lblDate);
		
		JLabel lblAmount = new JLabel("Amount :");
		lblAmount.setForeground(new Color(255, 255, 255));
		lblAmount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblAmount.setBounds(10, 131, 98, 14);
		contentPane.add(lblAmount);
		
		JLabel lblTypeOfAccount = new JLabel("Type of account :");
		lblTypeOfAccount.setForeground(new Color(255, 255, 255));
		lblTypeOfAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblTypeOfAccount.setBounds(10, 168, 171, 40);
		contentPane.add(lblTypeOfAccount);
		
		JRadioButton rdbtnSaving = new JRadioButton("Saving");
		rdbtnSaving.setBounds(170, 168, 109, 23);
		contentPane.add(rdbtnSaving);
		
		JRadioButton rdbtnSpending = new JRadioButton("Spending");
		rdbtnSpending.setBounds(170, 200, 109, 23);
		contentPane.add(rdbtnSpending);
		
		ButtonGroup group = new ButtonGroup();
	    group.add(rdbtnSaving);
	    group.add(rdbtnSpending);
	    
	    rdbtnSaving.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             savingChecked=true;
	          }           
	       });

	    rdbtnSpending.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             spendingChecked=true;
	          }           
	       });
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ((textField.getText().isEmpty()) ||(textField_1.getText().isEmpty()))
					JOptionPane.showMessageDialog(contentPane,"Data missing");
				else
					if (!textField.getText().matches("\\d{4}-\\d{2}-\\d{2}")){
						JOptionPane.showMessageDialog(contentPane, "Date format incorrect; must be yyyy-mm-dd");
						textField.setText(null);
					}
					else 
					if (hasLetter.matcher(textField_1.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField_1.setText(null);
					}
					else if (hasSpecialChar.matcher(textField_1.getText()).find()) {
						JOptionPane.showMessageDialog(contentPane, "Invalid character");
						textField_1.setText(null);
					}else {
				Account newAccount=new Account();
				int id=new Random().nextInt(100);
				if (savingChecked) {
					newAccount.createAccount(Integer.parseInt(textField_1.getText()), textField.getText(), "saving", id, c_id);
					JOptionPane.showMessageDialog(contentPane,newAccount.toString());
					try {
						newReport.createReport(user.getUid(),textField.getText() , "New Account Creation : " + newAccount.getAid());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//GO BACK TO PREVIOUS PRESENTATION CLASS
				}
				else if (spendingChecked) {
					newAccount.createAccount(Integer.parseInt(textField_1.getText()), textField.getText(), "spending", id, c_id);
					JOptionPane.showMessageDialog(contentPane,newAccount.toString());
					try {
						newReport.createReport(user.getUid(),textField.getText() , "New Account Creation : "+ newAccount.getAid());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//contentPane.setVisible(false);
					//GO BACK TO PREVIOUS PRESENTATION CLASS
				}				
					}
			}
		});
		
		btnCreate.setBounds(181, 253, 86, 23);
		contentPane.add(btnCreate);
		
		textField = new JTextField();
		textField.setBounds(181, 93, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(181, 131, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 11, 79, 29);
		textPane.setText("Client: "+client.getCid());
		contentPane.add(textPane);
		
		JButton btnBack_1 = new JButton("Back");
		btnBack_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Clients2 newFrame=new Clients2(client, user);
				newFrame.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		btnBack_1.setBounds(335, 11, 89, 23);
		contentPane.add(btnBack_1);
	}
}
