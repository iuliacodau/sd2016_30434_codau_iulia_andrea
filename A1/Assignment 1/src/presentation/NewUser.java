package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businesslogic.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class NewUser extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;//WHAT DOES THIS DO?		
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private boolean adminChecked=false;
	private boolean employeeChecked=false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewUser frame = new NewUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewUser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCreateNewUser = new JLabel("Create New User");
		lblCreateNewUser.setForeground(new Color(255, 255, 255));
		lblCreateNewUser.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		lblCreateNewUser.setBounds(115, 24, 210, 33);
		contentPane.add(lblCreateNewUser);
		
		textField = new JTextField();
		textField.setBounds(149, 80, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(149, 129, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setForeground(new Color(255, 255, 255));
		lblUsername.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblUsername.setBounds(46, 83, 290, 14);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblPassword.setBounds(46, 132, 290, 14);
		contentPane.add(lblPassword);
		
		final JRadioButton rdbtnAdmin = new JRadioButton("Admin");
		rdbtnAdmin.setBounds(149, 174, 86, 23);
		contentPane.add(rdbtnAdmin);
		
		final JRadioButton rdbtnEmployee = new JRadioButton("Employee");
		rdbtnEmployee.setBounds(149, 215, 86, 23);
		contentPane.add(rdbtnEmployee);
		
		ButtonGroup group = new ButtonGroup();
	    group.add(rdbtnAdmin);
	    group.add(rdbtnEmployee);
	    
	    rdbtnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             adminChecked=true;
	          }           
	       });

	    rdbtnEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             employeeChecked=true;
	          }           
	       });
	    
		JButton btnCreate = new JButton("Create!");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				User newUser=new User();
				int id=new Random().nextInt(100);
				if (adminChecked) {
					newUser.createUser(id, "admin", textField.getText(), textField_1.getText());
					JOptionPane.showMessageDialog(contentPane,newUser.toString());
					contentPane.setVisible(false);
					//GO BACK TO PREVIOUS PRESENTATION CLASS
				}
				else if (employeeChecked) {
					newUser.createUser(id, "employee", textField.getText(), textField_1.getText());
					JOptionPane.showMessageDialog(contentPane,newUser.toString());
					contentPane.setVisible(false);
					//GO BACK TO PREVIOUS PRESENTATION CLASS
				}				
				
			}
		});
		btnCreate.setBounds(307, 142, 89, 23);
		contentPane.add(btnCreate);
	}
}
