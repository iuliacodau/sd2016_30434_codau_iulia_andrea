package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businesslogic.Client;
import businesslogic.Report;
import businesslogic.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class NewClient extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JButton btnBack;

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewClient frame = new NewClient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public NewClient(final User u) {
		final Report newReport=new Report();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setForeground(new Color(255, 255, 255));
		lblName.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblName.setBounds(53, 45, 127, 14);
		contentPane.add(lblName);
		
		JLabel lblCnp = new JLabel("CNP:");
		lblCnp.setForeground(new Color(255, 255, 255));
		lblCnp.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblCnp.setBounds(53, 81, 79, 14);
		contentPane.add(lblCnp);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setForeground(new Color(255, 255, 255));
		lblAddress.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblAddress.setBounds(53, 110, 79, 39);
		contentPane.add(lblAddress);
		
		JLabel lblIdentityCard = new JLabel("ID:");
		lblIdentityCard.setForeground(new Color(255, 255, 255));
		lblIdentityCard.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblIdentityCard.setBounds(53, 168, 103, 23);
		contentPane.add(lblIdentityCard);
		
		textField = new JTextField();
		textField.setBounds(175, 42, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(175, 80, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(175, 121, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(175, 171, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client newClient=new Client();

				if ((textField.getText().isEmpty()) || 		
				(textField_1.getText().isEmpty()) ||
				(textField_2.getText().isEmpty()) ||
				(textField_3.getText().isEmpty())) 
					JOptionPane.showMessageDialog(contentPane, "Empty fields!");
				else {
					
					if (!newClient.checkName(textField.getText())) {
						JOptionPane.showMessageDialog(contentPane, "Wrong input for name");
						textField.setText(null);
					} else
					if (!newClient.checkCNP(textField_1.getText())){
						JOptionPane.showMessageDialog(contentPane, "Wrong input for CNP");
						textField_1.setText(null);
					} else
					if (!newClient.checkId(textField_3.getText())){
						JOptionPane.showMessageDialog(contentPane, "Wrong input for ID");
						textField_3.setText(null);
					} else {
					
					
					boolean check=newClient.createClient(u.getUid(), textField.getText(), textField_1.getText(), textField_2.getText(), textField_3.getText());
					if (!check){
						JOptionPane.showMessageDialog(contentPane, "CNP already in database=client exists!");
					}else {
					JOptionPane.showMessageDialog(contentPane, "Client added");
					Date DateOfToday=new Date();
					DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
					String today=df.format(DateOfToday);
					try {
						newReport.createReport(u.getUid(),today , "New Client Creation");
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					textField.setText(null);	
					textField_1.setText(null);
					textField_2.setText(null);
					textField_3.setText(null);
				}}
			}
				}
		});
		btnCreate.setBounds(290, 98, 109, 23);
		contentPane.add(btnCreate);
		
		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clients back=new Clients(u);
				back.setVisible(true);
				dispose();
			}
		});
		btnBack.setBounds(21, 228, 89, 23);
		contentPane.add(btnBack);
	}

}
