package presentation;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businesslogic.Client;
import businesslogic.User;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JTextPane;

public class Clients extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private ArrayList<Client> listOfClients=new ArrayList<Client>();
	private Client selectedClient=new Client();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clients frame = new Clients(new User());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Clients(final User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 645, 551);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHello = new JLabel("Hello " + user.getUsername() +" "+ user.getUid());
		lblHello.setForeground(new Color(255, 255, 255));
		lblHello.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		lblHello.setBounds(209, 125, 247, 39);
		contentPane.add(lblHello);
		
		JLabel lblHereAreYour = new JLabel("Here are your clients");
		lblHereAreYour.setForeground(new Color(255, 255, 255));
		lblHereAreYour.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblHereAreYour.setBounds(209, 187, 221, 31);
		contentPane.add(lblHereAreYour);
		
		final DefaultTableModel model;
		model=new DefaultTableModel();
		model.addColumn("Client ID");
		model.addColumn("User ID");
		model.addColumn("Name");
		model.addColumn("CNP");
		model.addColumn("Address");
		model.addColumn("Identity Card");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(30, 254, 564, 152);
		contentPane.add(scrollPane);
		table = new JTable(model);
		scrollPane.setViewportView(table);
		
		Client mockClient=new Client();
		listOfClients=mockClient.findAllClients(user.getUid());
		for (int i=0; i<listOfClients.size();i++){
			int cid=listOfClients.get(i).getCid();
			int uid=listOfClients.get(i).getUid();
			String name=listOfClients.get(i).getName();
			String cnp=listOfClients.get(i).getCnp();
			String address=listOfClients.get(i).getAddress();
			String cardid=listOfClients.get(i).getCardid();
			model.addRow(new Object[]{cid, uid, name, cnp, address, cardid});
		}
		
		/*
		String columnNames[] = {"Name", "CNP", "Address", "Identity Card"};
		Object rowData[][] = null;
		table = new JTable(rowData,columnNames);
		table.setBounds(35, 77, 365, 141);
		contentPane.add(table);
		*/
		JButton btnSelectClient = new JButton("Select Client");
		btnSelectClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selectedClient.setCid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 0).toString()));
				selectedClient.setUid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 1).toString()));
				selectedClient.setName((String) model.getValueAt(table.getSelectedRow(), 2).toString());
				selectedClient.setCnp((String) model.getValueAt(table.getSelectedRow(), 3).toString());
				selectedClient.setAddress((String) model.getValueAt(table.getSelectedRow(), 4).toString());
				selectedClient.setCardid((String) model.getValueAt(table.getSelectedRow(), 5).toString());
				Clients2 openWindow=new Clients2(selectedClient, user);
				openWindow.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		btnSelectClient.setBounds(225, 467, 139, 23);
		contentPane.add(btnSelectClient);
		
		JButton btnAddNewClient = new JButton("Add New Client");
		btnAddNewClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewClient nc=new NewClient(user);
				nc.setVisible(true);
				//dispose();
			}
		});
		btnAddNewClient.setBounds(317, 433, 139, 23);
		contentPane.add(btnAddNewClient);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(13, 11, 182, 9);
		contentPane.add(separator);
		
		JTextPane textPane = new JTextPane();
		textPane.setText((String) null);
		textPane.setBounds(59, 20, 89, 23);
		textPane.setText(user.getUsername());
		textPane.setEditable(false);
		contentPane.add(textPane);
		
		JButton button = new JButton("Logout");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				Home homepage=new Home();
				homepage.getFrame().setVisible(true);
			}
		});
		button.setBounds(59, 54, 89, 23);
		contentPane.add(button);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 80, 182, 9);
		contentPane.add(separator_1);
		
		JButton btnDeleteClient = new JButton("Delete Client");
		btnDeleteClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(table.getSelectedRow());
				Client clientToDelete=new Client();
				clientToDelete.setCid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 0).toString()));
				clientToDelete.setUid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 1).toString()));
				clientToDelete.setName((String) model.getValueAt(table.getSelectedRow(), 2).toString());
				clientToDelete.setCnp((String) model.getValueAt(table.getSelectedRow(), 3).toString());
				clientToDelete.setAddress((String) model.getValueAt(table.getSelectedRow(), 4).toString());
				clientToDelete.setCardid((String) model.getValueAt(table.getSelectedRow(), 5).toString());
				clientToDelete.deleteClient();
				System.out.println(clientToDelete.getName());
				System.out.println(clientToDelete.toString());
				model.removeRow(table.getSelectedRow());
				//textField.setText(null);
				//textField_1.setText(null);
				//group.clearSelection();
			}
		});
		btnDeleteClient.setBounds(135, 433, 139, 23);
		contentPane.add(btnDeleteClient);
	}
}
