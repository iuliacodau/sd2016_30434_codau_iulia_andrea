package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businesslogic.Client;
import businesslogic.Report;
import businesslogic.User;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

public class UpdateClient extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateClient frame = new UpdateClient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public UpdateClient(final Client c, final User u) {
		final Report newReport=new Report();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBackground(new Color(102, 0, 102));
		panel.setBounds(0, 0, 434, 262);
		contentPane.add(panel);
		
		JLabel label = new JLabel("Name:");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		label.setBounds(53, 45, 127, 14);
		panel.add(label);
		
		JLabel label_1 = new JLabel("CNP:");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		label_1.setBounds(53, 81, 79, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Address:");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		label_2.setBounds(53, 110, 79, 39);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("Identity:");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		label_3.setBounds(53, 168, 103, 23);
		panel.add(label_3);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(175, 42, 86, 20);
		panel.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(175, 80, 86, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(175, 121, 86, 20);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(175, 171, 86, 20);
		panel.add(textField_3);
		
		textField.setText(c.getName());
		textField_1.setText(c.getCnp());
		textField_2.setText(c.getAddress());
		textField_3.setText(c.getCardid());
		
		JButton button = new JButton("Update");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client newClient=new Client();
				if ((textField.getText().isEmpty()) || 		
				(textField_1.getText().isEmpty()) ||
				(textField_2.getText().isEmpty()) ||
				(textField_3.getText().isEmpty())) 
					JOptionPane.showMessageDialog(contentPane, "Can't have empty fields!");
				else {
				if (!newClient.checkName(textField.getText())) {
					JOptionPane.showMessageDialog(contentPane, "Wrong input for name");
					//textField.setText(null);
					textField.setText(c.getName());
				} else 
				if (!newClient.checkCNP(textField_1.getText())){
					JOptionPane.showMessageDialog(contentPane, "Wrong input for CNP");
					//textField_1.setText(null);
					textField_1.setText(c.getCnp());
				} else 
				if (!newClient.checkId(textField_3.getText())){
					JOptionPane.showMessageDialog(contentPane, "Wrong input for ID");
					//textField_3.setText(null);
					textField_3.setText(c.getCardid());
				}

				else {
					newClient.updateClient(c.getCid(), c.getUid(), textField.getText(), textField_1.getText(), textField_2.getText(), textField_3.getText());
					JOptionPane.showMessageDialog(contentPane, "Client updated");
					Date DateOfToday=new Date();
					DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
					String today=df.format(DateOfToday);
					try {
						newReport.createReport(u.getUid(),today , "Updated Client with id : "+ c.getCid());
					} catch (ParseException exc) {
						// TODO Auto-generated catch block
						exc.printStackTrace();
					}
					textField.setText(null);	
					textField_1.setText(null);
					textField_2.setText(null);
					textField_3.setText(null);
				}
				}
			}
		});
		button.setBounds(290, 98, 109, 23);
		panel.add(button);
		
		JButton button_1 = new JButton("Back");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		button_1.setBounds(21, 228, 89, 23);
		panel.add(button_1);
	}
}
