package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import businesslogic.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.JTextField;

public class Home {

	private JFrame frame;
	private JTextField textFieldUsername;
	private JTextField textFieldPassword;

	private boolean userNotFound=false;
	private boolean wrongPassword=false;
	//private User user1=new User();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home window = new Home();
					window.getFrame().setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Home() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().getContentPane().setBackground(new Color(105, 105, 105));
		getFrame().setBounds(100, 100, 457, 302);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);


		final JPanel main = new JPanel();
		main.setBackground(new Color(102, 0, 102));
		main.setBounds(0, 0, 441, 264);
		getFrame().getContentPane().add(main);
		main.setLayout(null);
		main.setVisible(true);

		JLabel label = new JLabel("Welcome");
		label.setBounds(117, 46, 180, 43);
		label.setForeground(new Color(255, 255, 255));
		label.setFont(new Font("Broadway", Font.PLAIN, 37));
		main.add(label);

		final JPanel enter = new JPanel();
		enter.setForeground(new Color(255, 255, 255));
		enter.setBackground(new Color(102, 0, 102));
		enter.setBounds(0, 0, 441, 264);
		getFrame().getContentPane().add(enter);
		enter.setLayout(null);
		enter.setVisible(false);

		JButton button = new JButton("Enter Account");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				enter.setVisible(true);
				main.setVisible(false);
			}
		});
		button.setBounds(130, 163, 156, 23);
		button.setBackground(Color.WHITE);
		main.add(button);



		JButton btnNewButton = new JButton("Enter");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username=new String();
				String password=new String();
				username=textFieldUsername.getText();
				password=textFieldPassword.getText();
				if ((username.isEmpty())||(password.isEmpty())) {
					JOptionPane.showMessageDialog(getFrame().getContentPane(),"Empty field!");
				}else {
					User userDatabase=new User();
					User userInput=new User();
					userInput.setUsername(username);
					userInput.setPassword(password);
					userNotFound=true;
					wrongPassword=true;
					for (int i=0; i<userDatabase.findAllUsers().size(); i++ )
					{
						if (username.equals(userDatabase.findAllUsers().get(i).getUsername()))
						{
							System.out.println("User exists");
							userNotFound=false;
							if (userInput.checkPassword(password,username)){
								wrongPassword=false;
								userInput=userInput.findUserByUsername(username);
								if (userInput.isAdmin()) {
									getFrame().dispose();
									Admin adminFrame = new Admin(userInput);
									adminFrame.setVisible(true);
								}else{
									getFrame().dispose();
									Clients clientFrame = new Clients(userInput);
									clientFrame.setVisible(true);
								}
							}
						}else {
							textFieldUsername.setText(null);
							textFieldPassword.setText(null);
						}
					} 

					if (userNotFound)
						JOptionPane.showMessageDialog(getFrame().getContentPane(),"Inexistent user");
					else if (wrongPassword)
						JOptionPane.showMessageDialog(getFrame().getContentPane(),"Wrong password");
				}
			}
		});
		btnNewButton.setBounds(183, 208, 89, 23);
		enter.add(btnNewButton);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblUsername.setForeground(new Color(255, 255, 255));
		lblUsername.setBounds(128, 101, 113, 14);
		enter.add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		lblPassword.setBounds(137, 140, 76, 14);
		enter.add(lblPassword);

		JLabel lblEnterAccount = new JLabel("Enter account");
		lblEnterAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		lblEnterAccount.setForeground(new Color(255, 255, 255));
		lblEnterAccount.setBounds(137, 24, 203, 37);
		enter.add(lblEnterAccount);

		textFieldUsername = new JTextField();
		textFieldUsername.setBounds(223, 100, 86, 20);
		enter.add(textFieldUsername);
		textFieldUsername.setColumns(10);

		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(223, 139, 86, 20);
		enter.add(textFieldPassword);
		textFieldPassword.setColumns(10);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
