package presentation;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businesslogic.Report;
import businesslogic.User;
import businesslogic.WriteFile;

import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import javax.swing.JSeparator;

public class Admin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private boolean adminChecked=false;
	private boolean employeeChecked=false;
	private ArrayList<User> listOfUsers=new ArrayList<User>();
	private User selectedUser=new User();

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin(new User());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin(final User user) {
		
		final Report newReport=new Report();
		setFrame(new JFrame());
		
		getFrame().getContentPane().setBackground(new Color(102, 0, 102));
		getFrame().setBounds(100, 100, 535, 572);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 535, 572);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		getFrame().getContentPane().add(contentPane);
		setContentPane(contentPane);
		contentPane.setLayout(null);
	

		
		final DefaultTableModel model;
		model=new DefaultTableModel();
		model.addColumn("User ID");
		model.addColumn("Type");
		model.addColumn("Username");
		model.addColumn("Password");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 49, 332, 144);
		contentPane.add(scrollPane);
		table = new JTable(model);
		scrollPane.setViewportView(table);
		
		final JRadioButton radioButton = new JRadioButton("Admin");
		radioButton.setBounds(149, 390, 109, 23);
		contentPane.add(radioButton);
		
		final JRadioButton radioButton_1 = new JRadioButton("Employee");
		radioButton_1.setBounds(149, 431, 109, 23);
		contentPane.add(radioButton_1);
		
		final ButtonGroup group = new ButtonGroup();
	    group.add(radioButton);
	    group.add(radioButton_1);
	    
	    radioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             adminChecked=true;
	             employeeChecked=false;
	          }           
	       });

	    radioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {       
	             employeeChecked=true;
	             adminChecked=false;
	          }           
	       });

		listOfUsers=user.findAllUsers();
		for (int i=0; i<listOfUsers.size();i++){
			int uid=listOfUsers.get(i).getUid();
			String type=listOfUsers.get(i).getType();
			String username=listOfUsers.get(i).getUsername();
			String password=listOfUsers.get(i).getPassword();
			model.addRow(new Object[]{uid,type,username,password});
		}

		
		JLabel lblAllUsers = new JLabel("All Users");
		lblAllUsers.setForeground(new Color(255, 255, 255));
		lblAllUsers.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		lblAllUsers.setBounds(199, 11, 134, 33);
		contentPane.add(lblAllUsers);
		
		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selectedUser.setUid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 0).toString()));
				selectedUser.setType((String) model.getValueAt(table.getSelectedRow(), 1).toString());
				selectedUser.setUsername((String) model.getValueAt(table.getSelectedRow(), 2).toString());
				selectedUser.setPassword((String) model.getValueAt(table.getSelectedRow(), 3).toString());
				textField.setText(model.getValueAt(table.getSelectedRow(), 2).toString());
				textField_1.setText(model.getValueAt(table.getSelectedRow(), 3).toString());
				if (selectedUser.getType().equals("admin")) {
					radioButton.setSelected(true);
					radioButton_1.setSelected(false);
					adminChecked=true;
					employeeChecked=false;
				}
				if (selectedUser.getType().equals("employee")) {
					radioButton.setSelected(false);
					radioButton_1.setSelected(true);
					adminChecked=false;
					employeeChecked=true;
				}
			}
		});
		btnSelect.setBounds(376, 67, 133, 23);
		contentPane.add(btnSelect);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(table.getSelectedRow());
				User userToDelete=new User();
				userToDelete.setUid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 0).toString()));
				userToDelete.setType((String) model.getValueAt(table.getSelectedRow(), 1).toString());
				userToDelete.setUsername((String) model.getValueAt(table.getSelectedRow(), 2).toString());
				userToDelete.setPassword((String) model.getValueAt(table.getSelectedRow(), 3).toString());
				userToDelete.deleteUser();
				System.out.println(userToDelete.getPassword());
				System.out.println(userToDelete.toString());
				model.removeRow(table.getSelectedRow());
				textField.setText(null);
				textField_1.setText(null);
				group.clearSelection();
			}
		});
		btnDelete.setBounds(376, 119, 133, 23);
		contentPane.add(btnDelete);
		
		JLabel label = new JLabel("Username");
		label.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		label.setForeground(new Color(255, 255, 255));
		label.setBounds(40, 293, 86, 63);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(149, 316, 109, 20);
		contentPane.add(textField);
		
		JLabel label_1 = new JLabel("Password");
		label_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
		label_1.setForeground(new Color(255, 255, 255));
		label_1.setBounds(40, 357, 115, 20);
		contentPane.add(label_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(149, 359, 109, 20);
		contentPane.add(textField_1);
		

		
		JButton btnUpdateFields = new JButton("Update fields");
		btnUpdateFields.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean noError=false;
				selectedUser.setUid(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 0).toString()));
				if (adminChecked)
					selectedUser.setType("admin");
				if (employeeChecked)
					selectedUser.setType("employee");
				if (textField.getText()!=null)
					selectedUser.setUsername(textField.getText());
				if (selectedUser.validPassword(textField_1.getText()))
					selectedUser.setPassword(textField_1.getText());
				else
					JOptionPane.showMessageDialog(contentPane, "INVALID! Password not changed");
				noError=selectedUser.updateUser(selectedUser.getUid(), selectedUser.getType(), selectedUser.getUsername(), selectedUser.getPassword());
				if (!noError) {
					JOptionPane.showMessageDialog(contentPane, "Username exists already");
				} else {
					model.setValueAt(selectedUser.getType(), table.getSelectedRow(), 1);
					model.setValueAt(selectedUser.getUsername(), table.getSelectedRow(), 2);
					model.setValueAt(selectedUser.getPassword(), table.getSelectedRow(), 3);
				}
				textField.setText(null);
				textField_1.setText(null);
				group.clearSelection();
			}
		});
		btnUpdateFields.setBounds(330, 359, 133, 23);
		contentPane.add(btnUpdateFields);
		
		JButton btnCreateNew = new JButton("Create New");
		btnCreateNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				User newUser=new User();
				System.out.println((textField.getText()));
				boolean noError=false;
				if ((textField.getText().isEmpty()) || (textField_1.getText().isEmpty()) || ((!adminChecked) && (!employeeChecked)))
					JOptionPane.showMessageDialog(contentPane, "Incomplete fields!");
				else {
					//newUser.setUid(id);
					newUser.setUsername(textField.getText());
					if (!newUser.validPassword(textField_1.getText())) {
						JOptionPane.showMessageDialog(contentPane, "Password must have 8 characters, at least one"
								+ "uppercase, one lowercase, one special character and one digit");
						textField_1.setText(null);
						return;
					}
						else {
						    newUser.setPassword(textField_1.getText());
						    JOptionPane.showMessageDialog(contentPane, "Valid Password");
					if (adminChecked) {
						newUser.setType("admin");
						noError=newUser.createUser(newUser.findId( textField.getText()), "admin", textField.getText(), textField_1.getText());
					}
					else if (employeeChecked) {
						newUser.setType("employee");
						System.out.println(newUser.findId( textField.getText()));
						noError=newUser.createUser(newUser.findId( textField.getText()), "employee", textField.getText(), textField_1.getText());
					}
					if (noError) {
					JOptionPane.showMessageDialog(contentPane,newUser.toString());
					model.addRow(new Object[]{newUser.getUid(), newUser.getType(), newUser.getUsername(), newUser.getPassword()});
					} else {
						JOptionPane.showMessageDialog(contentPane,"User already exists");
					}
					textField.setText(null);
					textField_1.setText(null);
					group.clearSelection();
				}	
				}
			}
		});
		btnCreateNew.setBounds(330, 402, 133, 23);
		contentPane.add(btnCreateNew);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Home newWindow=new Home();
				newWindow.getFrame().setVisible(true);
				setVisible(false);
				dispose();
				
			}
		});
		btnBack.setBounds(10, 500, 89, 23);
		contentPane.add(btnBack);
		
		JButton btnGenerateReports = new JButton("Reports");
		btnGenerateReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s1, s2;
				s1=JOptionPane.showInputDialog("Start Date [in the format: yyyy-mm-dd]");
				s2=JOptionPane.showInputDialog("End Date [in the format: yyyy-mm-dd]");
				if (!s1.matches("\\d{4}-\\d{2}-\\d{2}"))
					JOptionPane.showMessageDialog(contentPane, "Start date format incorrect");
				else 
					if (!s2.matches("\\d{4}-\\d{2}-\\d{2}"))
					JOptionPane.showMessageDialog(contentPane, "End date format incorrect");
				else {
				Random rn=new Random();
				int number=rn.nextInt(1000)+1;
				ArrayList<Report> reports=new ArrayList<Report>();
				//System.out.println(model.getValueAt(table.getSelectedRow(), 0));
				if (table.getSelectedRow()==-1)
					JOptionPane.showMessageDialog(contentPane, "You have to select a user");
				else {
				try {
					reports=newReport.findAllReports(Integer.parseInt((String) model.getValueAt(table.getSelectedRow(), 0).toString()),s1,s2);
				} catch (NumberFormatException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (ParseException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				System.out.println("Reports "+ reports.size());
				try {
					WriteFile data=new WriteFile("C:/Users/Iulia/workspace/Bank/report"+number+".txt", true);
					data.writeToFile("REPORT GENERATED");
					if (reports.size()<1)
						data.writeToFile("No reports found");
					else{
					for (int i=0; i<reports.size();i++) {
						data.writeToFile(reports.get(i).toString());
					}
					}
					data.writeToFile("END OF REPORT");
					//Process p=Runtime.getRuntime().exec("C:/Windows/system32/notepad.exe"+"report.txt");
					Desktop.getDesktop().open(new File("C:/Users/Iulia/workspace/Bank/report"+number+".txt"));
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			}
			}
		});
		btnGenerateReports.setBounds(378, 170, 131, 23);
		contentPane.add(btnGenerateReports);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(30, 246, 468, 23);
		contentPane.add(separator);
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
