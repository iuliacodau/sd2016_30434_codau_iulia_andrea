package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import businesslogic.Client;

public class ClientCreateTest {

	@Test
	public void test() {
		Client client=new Client();
		client.createClient(26, "Alina Negoescu", "2961121657843", "Strada Salcamilor 25", "BV345267");
		Client c2=new Client();
		c2=c2.findClientByCnp("2961121657843");
		assertEquals("Alina Negoescu", c2.getName());
		assertEquals("BV345267", c2.getCardid());	
	}

}
