package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import businesslogic.Client;

public class CreateExistingClientTest {

	@Test
	public void test() {
		Client client=new Client();
		assertEquals(false, client.createClient(26, "Jane Doe", "2940923260051", "Strada Salcamilor 25", "MS345267"));	
	}

}
