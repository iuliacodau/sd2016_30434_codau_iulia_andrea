package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import businesslogic.Client;

public class ClientUpdateTest {

	@Test
	public void test() {
		Client c2=new Client();
		c2=c2.findClientByName("Camelia Tira");
		c2.updateClient(c2.getCid(), c2.getUid(), "Camelia Fira", c2.getCnp(), c2.getAddress(), c2.getCardid());
		assertEquals("Camelia Fira", c2.getName());	
	}

}
