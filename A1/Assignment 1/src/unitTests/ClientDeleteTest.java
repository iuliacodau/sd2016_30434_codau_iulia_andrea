package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import businesslogic.Client;

public class ClientDeleteTest {

	@Test
	public void test() {
		Client c2=new Client();
		c2=c2.findClientByCnp("2961121657843");
		c2.deleteClient(c2.getCid());
		Client c=new Client();
		assertEquals(null, c.findClientByCnp("2961121657843").getCnp());
	}

}
